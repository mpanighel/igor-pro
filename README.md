# igor-pro

General purpose procedures to analyse data and extend functionalities of Wavemetrics Igor Pro 6 and 7.

## Installation

Copy any of the .ipf files into "User files/Igor Procedures".

## Usage
A menu "MP" will appear in Igor Pro.
See "MP>[Procedure name]>Help" for information on how to use them.
