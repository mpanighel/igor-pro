//
// Read MP>Xray>Help
//

#pragma rtGlobals=3		// Use modern global access method and strict wave access.
#pragma version=1.0

Function MP_XRayHelp()
// XRay
// Version 1.1
// by Mirko Panighel (mirkopanighel {at} gmail {dot} com)
// _____________________________________________

// XPS, NEXAFS, ARPES and XMCD functions and extensions for IgoPro

// Use:
// All functions are accessible in the "MP>Xray" menu or
// by right-clicking on a trace in a graph.

// Help:

// Menu "XRay"
// 	"NEXAFS angle calculator", Calculate tilt angle of orbitals from NEXAFS intensities at two angles
// 	"ARPES Theta2k", Transform ARPES images from degrees to wave vector
// 	"Help", Show this help

// Menu "TracePopup" (appears by right-clicking on a trace in a graph)
// 	SubMenu "XRay"
// 		SubMenu "Shirley background"
// 			"Calculate", Calculate the Shirley background of the trace between the cursors
// 			"Subtract", /Q, Calculate and substract the Shirley background between the cursors from a copy of the trace
// 		SubMenu "NEXAFS step background"
// 			"Calculate", Calculate a NEXAFS "step function" between the cursors
// 		SubMenu "Normalize XAS"
// 			"Edges on cursors [point]", Set CursorA Y value to zero and CursorB Y value to one
// 			"Edges on cursors [line fit]", Set the pre-edge line fit from first point to CursorA to Y=0 and from CursorB to the end to Y=1
// 		SubMenu "XMCD", Put CursorA on u+ trace and CursorB on u- trace and calculate XMCD...
// 			"Edges on cursors [point]", ...setting CursorA Y value to zero and CursorB Y value to one
// 			"Edges on cursors [line fit]", ...setting the pre-edge line fit from first point to CursorA to Y=0 and from CursorB to the end to Y=1
// 			"No background subtraction", ...without any normalization
// 		"Mark peak on CursorA", Add a vertical line at the position of CursorA (CTRL+5)
// 		"Mark peak on maximum", Add a vertical line at the maximum of the trace
// 		SubMenu "Modify Graph"
// 			"XPS", Graph appearance for XPS
// 			"NEXAFS", Graph appearance for NEXAFS
// 			"XMCD", Graph appearance for XMCD

// Changelog:

// v1.1	Bug fixes:
// 		- Corrected the algorithm to calculate/subtract Shirley background from X-scaled waves.
// 		New features:
// 		- Added transformation of ARPES images from degrees to wave vector (XRay>ARPES Theta2k)
End

Menu "TracePopup"
SubMenu "MP"
	SubMenu "XRay"
		SubMenu "Shirley background"
			"Calculate", /Q, MP_CalcShirley()
			"Subtract", /Q, MP_SubShirley()
		End
		SubMenu "NEXAFS step background"
			"Calculate", /Q, MP_CalcStep()
		End
		SubMenu "Normalize XAS"
			"Edges on cursors [point]", /Q, MP_NormXAS(0)
			"Edges on cursors [line fit]", /Q, MP_NormXAS(1)
		End
		SubMenu "XMCD"
			"Edges on cursors [point]", /Q, MP_XMCD(0)
			"Edges on cursors [line fit]", /Q, MP_XMCD(1)
			"No background subtraction", /Q, MP_XMCD(2)
		End
		"Mark peak on CursorA/5", /Q, MP_PeakPosition(0)
		"Mark peak on maximum", /Q, MP_PeakPosition(1)
		SubMenu "Modify Graph"
			"XPS", /Q, MP_ModifyXPS()
			"NEXAFS", /Q, MP_ModifyNEXAFS()
			"XMCD", /Q, MP_ModifyXMCD()
		End
	End
End
End

Menu "MP"
	SubMenu "XRay"
		"NEXAFS angle calculator", /Q, MP_NEXAFSang()
		"ARPES Theta2k", /Q, MP_Theta2k()
		"Help", /Q, MP_DisplayXrayHelp()
	End
End

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Normalize to pre- and post-edge lines///////////////////////////////////////////////////////////////////////////////////////////////
//cursorA is the pre-edge;
//cursorB is the post-edge;
//if line=1 pre and post-edge are fitted with a line
//otherwise pre=0 and post=1
Function MP_NormXAS(line)
	
	Variable line
	
	String dfA = GetWavesDataFolder(CsrWaveRef(A), 2)
	String dfB = GetWavesDataFolder(CsrWaveRef(B), 2)
	if (CmpStr(dfA, dfB) != 0)
		Abort "Both cursors must be on the same wave."
	endif
	
	SetDataFolder GetWavesDataFolder(CsrWaveRef(A), 1)
	
	Variable pre=pcsr(A)
	Variable post=pcsr(B)
	
	Wave wy=CsrWaveRef(A)
	Wave wx=CsrXWaveRef(A)
	
	Variable N=numpnts(wy)
		
	Wave fit_wy_tmp
	Wave W_coef
	
	Duplicate/O wy wy_tmp
	
	if (line==0)
		Variable wy_base=wy_tmp[pre]
		wy_tmp-=wy_base
		Variable wy_nor=wy_tmp[post]
		wy_tmp/=wy_nor
	else
		if(pre<2)
			Abort "Too few points before pre-edge"
		endif
		if(post>N-3)
			Abort "Too few points after post-edge"
		endif
		
		Duplicate/O wy_tmp pre_tmp, post_tmp, fit_wy_tmp
		if(WaveExists(wx))
			CurveFit/L=(N) /X=1/NTHR=0 line  wy_tmp[0,pre] /X=wx /D=fit_wy_tmp
		else
			CurveFit/L=(N) /X=1/NTHR=0 line  wy_tmp[0,pre] /D=fit_wy_tmp
		endif
		pre_tmp= W_coef[0]+W_coef[1]*wx[p]
		
		if(WaveExists(wx))
			CurveFit/L=(N) /X=1/NTHR=0 line  wy_tmp[post,N-1] /X=wx /D=fit_wy_tmp
		else
			CurveFit/L=(N) /X=1/NTHR=0 line  wy_tmp[post,N-1] /X=wx /D=fit_wy_tmp
		endif
		post_tmp= W_coef[0]+W_coef[1]*wx[p]
		
		Variable step=post_tmp[pre]-pre_tmp[pre]

		wy_tmp-=pre_tmp
		wy_tmp/=step
	endif
	
	if(strsearch(NameOfWave(wy),"_norm",0)==-1)
		Wave wy_norm=$(GetWavesDataFolder(wy,2)+"_norm")
		CheckDisplayed/A wy_norm
		if(V_flag)
			Abort NameOfWave(wy_norm)+" is in use in another graph. Abort."
		else
			KillWaves/Z wy_norm
		endif
		Duplicate/O wy wy_orig
		Duplicate/O wy_tmp wy
		String wyname=NameOfWave(wy)+"_norm"
		wyname=MP_CheckWaveNameX(wyname, "Normalize wave")
		Rename wy $wyname
		Duplicate/O wy_orig $(RemoveEnding(GetWavesDataFolder(wy,2),"_norm"))
		KillWaves/Z wy_orig
	else
		Duplicate/O wy_tmp spectrum
	endif
	if(line==0)
		Print GetWavesDataFolder(wy,2)+"-="+num2str(wy_base)
		Print GetWavesDataFolder(wy,2)+"/="+num2str(wy_nor)
	else
		Print GetWavesDataFolder(wy,2)+"-="+NameOfWave(pre_tmp)
		Print GetWavesDataFolder(wy,2)+"/="+num2str(step)
	endif
	KillWaves/Z int_bkg, spectrum_tmp, pre_tmp, post_tmp
	
End

//Calculate XMCD///////////////////////////////////////////////////////////////////////////////////////////////
//Put curorA on pre-edge of u+ and cursorB on post-edge of u-
//line=0 baseline is on pre and normalization on post,
//line=1 pre and post-edge are fitted with a line (for fair background)
//line=2 no background substraction

Function MP_XMCD(line)
	
	Variable line
	
	SetDataFolder GetWavesDataFolder(CsrWaveRef(A), 1)
	
	String traceA=StringByKey("TNAME", CsrInfo(A))
	Wave pls=TraceNameToWaveRef("", traceA)
	Wave pls_E=XWaveRefFromTrace("", traceA)
	
	String traceB=StringByKey("TNAME", CsrInfo(B))
	Wave mns=TraceNameToWaveRef("", traceB)
	Wave mns_E=XWaveRefFromTrace("", traceB)
	
	Variable pre=pcsr(A)
	Variable post=pcsr(B)
	
	Duplicate/O pls pls_tmp
	Duplicate/O mns mns_tmp
	
	if(WaveExists(pls_E))
		Interpolate2/T=2/E=2/I=3/Y=mns_tmp/X=pls_E mns_E, mns
	else
		Interpolate2/T=2/E=2/I=3/Y=mns_tmp mns_E, mns
	endif
	
	Smooth 1, pls_tmp
	Smooth 1, mns_tmp
	
	Duplicate/O pls_tmp xmcd_tmp, mean_tmp, pre_tmp, post_tmp
	mean_tmp=(pls_tmp+mns_tmp)*0.5
	Variable N=numpnts(mean_tmp), step
	
	switch(line)
		case 0:
			Variable base=mean(pls_tmp,pnt2x(pls_tmp,pre-2),pnt2x(pls_tmp,pre+2))
			pls_tmp-=base
			Variable nrm=mean(pls_tmp,pnt2x(pls_tmp,post-2),pnt2x(pls_tmp,post+2))
			pls_tmp/=nrm
			
			base=mean(mns_tmp,pnt2x(mns_tmp,pre-2),pnt2x(mns_tmp,pre+2))
			mns_tmp-=base
			nrm=mean(mns_tmp,pnt2x(mns_tmp,post-2),pnt2x(mns_tmp,post+2))
			mns_tmp/=nrm
			break
		case 1:
			Wave fit_mean_tmp
			if(WaveExists(pls_E))
				CurveFit/Q/L=(N) /X=1/NTHR=0 line mean_tmp[0,pre] /X=pls_E /D
			else
				CurveFit/Q/L=(N) /X=1/NTHR=0 line mean_tmp[0,pre] /D
			endif
			Duplicate/O fit_mean_tmp pre_tmp
			if(WaveExists(pls_E))
				CurveFit/Q/L=(N) /X=1/NTHR=0 line mean_tmp[post,N-1] /X=pls_E /D
			else
				CurveFit/Q/L=(N) /X=1/NTHR=0 line mean_tmp[post,N-1] /D
			endif
			Duplicate/O fit_mean_tmp post_tmp
			
			step=abs(post_tmp[pre]-pre_tmp[pre])
			
			pls_tmp-=pre_tmp
			pls_tmp/=step
			
			mns_tmp-=pre_tmp
			mns_tmp/=step
			break
	endswitch
	
	mean_tmp=(pls_tmp+mns_tmp)*0.5
	mean_tmp-=min(0.1*WaveMax(pls_tmp),0.5)
	Duplicate/O mean_tmp sign_tmp
	WaveTransform/O sgn sign_tmp
	mean_tmp*=(sign_tmp+1)*0.5
	mean_tmp+=min(0.1*WaveMax(pls_tmp),0.5)
	
	//XMCD=(u+ - u-)/(u+ + u-):
	xmcd_tmp=(pls_tmp-mns_tmp)/mean_tmp
	
	//XMCD=(u+ - u-):
	//xmcd_tmp=(pls_tmp-mns_tmp)
	
	String plsname=NameOfWave(pls)+"_pls"
	plsname=MP_CheckWaveNameX(plsname, "u+ wave name")
	Duplicate/O pls_tmp $plsname
	
	String mnsname=NameOfWave(mns)+"_mns"
	mnsname=MP_CheckWaveNameX(mnsname, "u- wave name")
	Duplicate/O mns_tmp $mnsname
	
	String xmcdname=NameOfWave(pls)+"_xmcd"
	xmcdname=MP_CheckWaveNameX(xmcdname, "XMCD wave name")
	Duplicate/O xmcd_tmp $xmcdname
	
	if(WaveExists(pls_E))
		Display/L=Y1$plsname vs pls_E;DelayUpdate
		AppendToGraph/L=Y1 $mnsname vs mns_E;DelayUpdate
		AppendToGraph/L=Y2 $xmcdname vs pls_E;DelayUpdate
	else
		Display/L=Y1$plsname;DelayUpdate
		AppendToGraph/L=Y1 $mnsname;DelayUpdate
		AppendToGraph/L=Y2 $xmcdname;DelayUpdate
	endif
	ModifyGraph rgb($mnsname)=(0,0,0);DelayUpdate
	ModifyGraph axisEnab(Y1)={0,0.75};DelayUpdate
	ModifyGraph rgb($xmcdname)=(0,0,65280);DelayUpdate
	ModifyGraph axisEnab(Y2)={0.8,1};DelayUpdate
	ModifyGraph axisEnab(bottom)={0.15,1};DelayUpdate
	ModifyGraph mode($xmcdname)=7,useNegRGB($xmcdname)=1,usePlusRGB($xmcdname)=1;DelayUpdate
	ModifyGraph hbFill($xmcdname)=2,rgb($xmcdname)=(65535,65535,65535);DelayUpdate
	ModifyGraph negRGB($xmcdname)=(0,0,65280);DelayUpdate
	Label bottom "\\F'Times New Roman'Photon energy (eV)";DelayUpdate
	Label Y1 "\\F'Times New Roman'Absorption";DelayUpdate
	ModifyGraph lblPosMode(Y2)=1,lblMargin(Y2)=20;DelayUpdate
	Label Y2 "MCD";DelayUpdate
	ModifyGraph font="Times New Roman";DelayUpdate
	ModifyGraph lblPosMode(Y1)=1,lblMargin(Y1)=30;DelayUpdate
	ModifyGraph lsize($mnsname)=2;DelayUpdate
	ModifyGraph lsize($plsname)=2;DelayUpdate
	ModifyGraph lsize($xmcdname)=0;DelayUpdate
	Legend/C/N=text0/J/A=MC "\\F'Times New Roman'\rXAS Fe L\B2,3\M\r"+NameOfWave(pls)+", "+NameOfWave(mns)+"\r\\F'Symbol'\r\\s("+plsname+") m+\r\\s("+mnsname+") m-"
	
	KillWaves/Z pls_tmp, mns_tmp, xmcd_tmp, mean_tmp, sign_tmp, pre_tmp, post_tmp
	
End

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Calculate Shirley background///////////////////////////////////////////////////////////////////////////////////////////////
Function MP_CalcShirley()
	
	String dfA = GetWavesDataFolder(CsrWaveRef(A), 2)
	String dfB = GetWavesDataFolder(CsrWaveRef(B), 2)
	if (CmpStr(dfA, dfB) != 0)
		Abort "Both cursors must be on the same wave."
	endif
	
	SetDataFolder GetWavesDataFolder(CsrWaveRef(A), 1)
	
	Wave wy=CsrWaveRef(A)
	Wave wx=CsrXWaveRef(A)
	
	Duplicate/O wy shir, wy_tmp
	
	Variable N=numpnts(wy)
	Variable start
	Variable finish
	Variable step=1
	
	if(vcsr(B)>vcsr(A))
		start=pcsr(A)
		finish=pcsr(B)
	else
		start=pcsr(B)
		finish=pcsr(A)
	endif
	
	if(start>finish)
		step=-1
	endif
	
	Variable base=mean(wy, pnt2x(wy,start-2), pnt2x(wy,start+2))
	Variable nor=mean(wy, pnt2x(wy,finish-2), pnt2x(wy,finish+2))
	
	Variable i=start
	do
		if(WaveExists(wx))
			shir[i]=areaXY(wx,wy,wx[start],wx[i])
		else
			shir[i]=area(wy,DimOffset(wy,0)+DimDelta(wy,0)*start,DimOffset(wy,0)+DimDelta(wy,0)*i)
		endif
		i+=step
	while(i!=finish+step)
	
	Variable a=start, b=finish
	if(start>finish)
		a=finish
		b=start
	endif
	
	Variable int=shir[finish]
	print int
	shir[a,b]/=int
	shir[a,b]*=(nor-base)
	shir[a,b]+=base
	String shirbkg_name=NameOfWave(wy)+"_shirbkg_"+num2str(start)+"_"+num2str(finish)
	shirbkg_name=MP_CheckWaveNameX(shirbkg_name, "Shirley background")
	Duplicate/O shir $shirbkg_name
	if(WaveExists(wx))
		AppendToGraph $shirbkg_name vs wx
	else
		AppendToGraph $shirbkg_name
	endif
	
	KillWaves/Z shir
	
End

//Subtract Shirley background///////////////////////////////////////////////////////////////////////////////////////////////
Function MP_SubShirley()
	
	String dfA = GetWavesDataFolder(CsrWaveRef(A), 2)
	String dfB = GetWavesDataFolder(CsrWaveRef(B), 2)
	if (CmpStr(dfA, dfB) != 0)
		Abort "Both cursors must be on the same wave."
	endif
	
	SetDataFolder GetWavesDataFolder(CsrWaveRef(A), 1)
	
	Wave wy=CsrWaveRef(A)
	Wave wx=CsrXWaveRef(A)
	
	Duplicate/O wy shir, wy_tmp
	
	Variable N=numpnts(wy)
	Variable start
	Variable finish
	Variable step=1
	
	if(vcsr(B)>vcsr(A))
		start=pcsr(A)
		finish=pcsr(B)
	else
		start=pcsr(B)
		finish=pcsr(A)
	endif
	
	if(start>finish)
		step=-1
	endif
	
	Variable base=mean(wy, pnt2x(wy,start-2), pnt2x(wy,start+2))
	Variable nor=mean(wy, pnt2x(wy,finish-2), pnt2x(wy,finish+2))
	
	Variable i=start
	do
		if(WaveExists(wx))
			shir[i]=areaXY(wx,wy,wx[start],wx[i])
		else
			shir[i]=area(wy,DimOffset(wy,0)+DimDelta(wy,0)*start,DimOffset(wy,0)+DimDelta(wy,0)*i)
		endif
		i+=step
	while(i!=finish+step)
	
	Variable a=start, b=finish
	if(start>finish)
		a=finish
		b=start
	endif
	
	Variable int=shir[finish]
	print int
	shir[a,b]/=int
	shir[a,b]*=(nor-base)
	shir[a,b]+=base
	String shirbkg_name=NameOfWave(wy)+"_shirbkg_"+num2str(start)+"_"+num2str(finish)
	shirbkg_name=MP_CheckWaveNameX(shirbkg_name, "Shirley background")
	Duplicate/O shir $shirbkg_name
	if(WaveExists(wx))
		AppendToGraph $shirbkg_name vs wx
	else
		AppendToGraph $shirbkg_name
	endif
	
	wy_tmp-=shir
	String subshir_name=NameOfWave(wy)+"_subshir_"+num2str(start)+"_"+num2str(finish)
	subshir_name=MP_CheckWaveNameX(subshir_name, "Shirley subtracted")
	Duplicate/O wy_tmp $subshir_name
	if(WaveExists(wx))
		Display $subshir_name vs wx
	else
		Display $subshir_name
	endif
	
	KillWaves/Z shir, spectrum_tmp
	
End

Function/S MP_CheckWaveNameX(wvname, title)
	String wvname
	String title
	String newname=wvname
	if(strlen(newname)>31)
		Prompt newname, "Name of new wave (remove at least "+num2str(strlen(newname)-31)+" chars)"
		DoPrompt/HELP="" title, newname
		newname=MP_CheckWaveNameX(newname, title)
	endif
	Return newname
End

Function/S MP_CheckAddNameX(wvname)
	String wvname
	Variable num=str2num(wvname[strlen(wvname)-1])
	if(num==NaN)//Wave name not ending with number
		num=-1
	endif
	if(WaveExists($wvname))
		if(num==-1)
			wvname+="0"
		else
			wvname=wvname[0,strlen(wvname)-2]+num2str(num+1)
		endif
	wvname=MP_CheckAddNameX(wvname)
	endif
	Return wvname
End

Function MP_CalcStep()
	
	String dfA = GetWavesDataFolder(CsrWaveRef(A), 2)
	String dfB = GetWavesDataFolder(CsrWaveRef(B), 2)
	if (CmpStr(dfA, dfB) != 0)
		Abort "Both cursors must be on the same wave."
	endif
	
	SetDataFolder GetWavesDataFolder(CsrWaveRef(A), 1)
	
	Wave spectrum=CsrWaveRef(A)
	Wave energy=CsrXWaveRef(A)
	
	Duplicate/O spectrum step
	
	Variable N=numpnts(energy)
	Variable base=pcsr(A)
	Variable top=pcsr(B)
	
	Variable y0=spectrum[base]
	Variable height=spectrum[top]-spectrum[base]
	Variable pos=energy[top]
	Variable width=(energy[top]-energy[base])/10
	Variable decay=0.05
	
	MP_stepbkg(step,energy, y0, height, pos, width, decay)
	
	String stepbkg_name=NameOfWave(spectrum)+"_stepbkg_"+num2str(top)
	stepbkg_name=MP_CheckWaveNameX(stepbkg_name, "Step background")
	Duplicate/O step $stepbkg_name
	AppendToGraph $stepbkg_name vs energy
	
	Print "stepbkg("+stepbkg_name+","+NameOfWave(energy)+","+num2str(y0)+","+num2str(height)+","+num2str(pos)+","+num2str(width)+","+num2str(decay)+")"
	
	KillWaves/Z step
	
End

Function/WAVE MP_stepbkg(wv,energy, y0, height, pos, width, decay)
	
	Wave wv
	Wave energy
	Variable y0
	Variable height
	Variable pos
	Variable width
	Variable decay
	
	Variable i
	for(i=0;i<numpnts(energy);i+=1)
		if(energy[i]>pos+width)
			wv[i]=y0+height*(0.5+atan((energy[i]-pos)/(width/2))/pi)*exp(-decay*(energy[i]-pos-width))
		else
			wv[i]=y0+height*(0.5+atan((energy[i]-pos)/(width/2))/pi)
		endif
	endfor
	
End

Function MP_PeakPosition(flag)
	
	Variable flag
	
	Variable pos, int
	
	if(flag)
		GetLastUserMenuInfo
		Wave peak=TraceNameToWaveRef("", S_traceName)
		Wave energy=XWaveRefFromTrace("", S_traceName)
		WaveStats/M=1/Q peak
		pos=V_maxLoc
		int=V_max
	else
		Wave peak=CsrWaveRef(A)
		Wave energy=CsrXWaveRef(A)
		if(!WaveExists(peak))
			Abort "Cursor(A) not on the graph"
		endif
		pos=xcsr(A)
		int=peak[pcsr(A)]
	endif
	
	SetDataFolder GetWavesDataFolderDFR(peak)
	Duplicate/O peak wv
	wv=0
	wv[x2pnt(wv, pos)]=int
	String wvname=NameOfWave(peak)+"_"+ReplaceString("-", num2str(round(pos)), "m")
	wvname=MP_CheckWaveNameX(wvname, "Name of peak position wave")
	Duplicate/O wv $wvname
	if(WaveExists(energy))
		AppendToGraph $wvname vs energy
	else
		AppendToGraph $wvname
	endif
	ModifyGraph mode($wvname)=1, rgb($wvname)=(0,0,0)
	String tagname=UniqueName("mark", 14, 0, WinName(0, 1))
	Tag/C/N=$tagname/A=LB $wvname, pos, num2str(pos)+WaveUnits($wvname, 0)
	KillWaves/Z wv
	
End

//ARPES///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Function MP_Theta2k()
	
	DoAlert 0, "Please make sure the image is centered in angle."
	
	Wave inwave=MP_XRay_WaveDialog()
	if(!WaveExists(inwave))
		Abort
	endif
 
	String newname = NameofWave(inwave)+"_k"
	Duplicate/O inwave, $newname
	Wave outwave = $newname
 
	Variable rows,columns,xdelta,xoffset,ydelta,yoffset	// inwave parameters
	rows	= DimSize(inwave,0)
	columns	= DimSize(inwave,1)
	xdelta	= DimDelta(inwave,0)
	xoffset	= DimOffset(inwave,0)
	ydelta	= DimDelta(inwave,1)
	yoffset	= DimOffset(inwave,1)
	WaveStats/M=1/Q inwave
 	Variable contrast=V_max
 	
	Variable kmin,kmax,kdelta,Emax
	Emax	= xoffset + xdelta*(rows-1)
	kmin	= 0.512*sqrt(Emax)*sin(pi/180*(yoffset))	// calculate the k boundaries (i.e., k at highest Ekin)
	kmax	= 0.512*sqrt(Emax)*sin(pi/180*(yoffset+(columns-1)*ydelta))
	SetScale/I	y kmin,kmax,"Wave Vector [Ang^-1]", outwave		// scale the y axis
 
	outwave = interp2D(inwave, x, 180/pi*asin(y/ (0.512*sqrt(x)))) // recalculate to k
	outwave = (NumType(outwave)==2) ? 0 : outwave	// replace NaNs (optional)
	
	NewImage outwave
	ModifyImage ''#0 ctab= {*,contrast,Grays,0}
	
End

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Function MP_NEXAFSang()
	
	Variable theta1, i1, theta2, i2
	Prompt theta1, "Polarization incidence angle 1"
	Prompt i1, "Intensity 1"
	Prompt theta2, "Polarization incidence angle 2"
	Prompt i2, "Intensity 2"
	
	DoPrompt/HELP="" "Insert intensity and angles", theta1, i1, theta2, i2
	
	Make/O/N=(2,2) mA
	Make/O/N=2 mB
	
	mA[0][0]=(sin(theta1*pi/180))^2
	mA[0][1]=0.5*(cos(theta1*pi/180))^2
	mA[1][0]=(sin(theta2*pi/180))^2
	mA[1][1]=0.5*(cos(theta2*pi/180))^2
	
	mB[0]=i1
	mB[1]=i2
	
	MatrixOP/O mX=Inv(mA) x mB
	
	Variable phi=atan(sqrt(mX[0]/mX[1]))*180/pi
	
	Abort "Angle from the surface: "+num2str(phi)+ " degrees."
	
End

Function MP_ModifyXPS()
	
	ModifyGraph gFont="Times New Roman",gfSize=12;DelayUpdate
	ModifyGraph noLabel(left)=1;DelayUpdate
	Label left "Counts";DelayUpdate
	Label bottom "Binding Energy (eV)"
	
End

Function MP_ModifyNEXAFS()
	
	ModifyGraph gFont="Times New Roman",gfSize=12;DelayUpdate
	ModifyGraph noLabel(left)=1;DelayUpdate
	Label left "Absorption (a.u.)";DelayUpdate
	Label bottom "Photon Energy (eV)"
	
End

Function MP_ModifyXMCD()
	
	ModifyGraph gFont="Times New Roman",gfSize=12;DelayUpdate
	ModifyGraph noLabel(Y1)=1;DelayUpdate
	Label/Z Y1 "Absorption (a.u.)";DelayUpdate
	Label/Z left "Absorption (a.u.)";DelayUpdate
	Label/Z Y2 "MCD";DelayUpdate
	Label/Z bottom "Photon Energy (eV)"
	
End

Function MP_DisplayXrayHelp()
	DoWindow/K XrayHelpNB
	NewNotebook/K=1/N=XrayHelpNB/F=0/OPTS=3 as "Xray Help"
	String helpstr=ProcedureText("MP_XrayHelp")
	helpstr=RemoveListItem(0, helpstr, "\r")
	helpstr=RemoveListItem(ItemsInList(helpstr, "\r")-1, helpstr, "\r")
	helpstr=ReplaceString("// ", helpstr, "")
	Notebook XrayHelpNB, text=helpstr
End

Function/WAVE MP_XRay_WaveDialog()
	String wname=""
	Prompt wname, "2D wave", popup, WaveList("*",";","DIMS:2")
	DoPrompt "Select a wave in the current data folder", wname
	Return $wname
End