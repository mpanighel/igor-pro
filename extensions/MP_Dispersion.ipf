//
// Read MP>Dispersion>Help
//

#pragma rtGlobals=3		// Use modern global access method and strict wave access.
#pragma version=1.0

Function MP_DispersionHelp()
// Dispersion
// Version 1.0
// by Mirko Panighel (mirkopanighel {at} gmail {dot} com)
// _____________________________________________

// Calculate energy dispersion from a series of energy map images

// Use:
// All functions are accessible in the "MP>Dispersion" menu

// Help:

// Menu "Dispersion"
// 	"Calculate energy dispersion"
// 		1. In the dialog, select all the images to load
// 		2. In the prompt indicate:
// 		- the image size in nm (if not square, the horizontal one)
// 		- the name of the folder to create
// 		- optionally select a region of interest
// 		  (if "Yes" you will be prompted to select the region on the first loaded image);
// 		3. For each image, when prompted, indicate the corresponding energy in meV.
// 		4. When the dispersion panel appears:
// 		- adjust the angular direction "angle" and the integration range "+-"
// 		- contrast can be changed using "Contrast"; zoom is available in the graph and by "Reset Zoom"
// 		- use "Update" to update the graph
// 		- extract the 2D dispersion cuts by clicking "Checkpoint"
// 		- click "Done" when finished.
// 	"Show energy dispersion"
// 		Recall the dispersion panel (as point 4. above) after selecting a 3D energy wave from the current folder.
// 	"Help", Show this help
End

//MENUS////////////////////////////////////////////////////////
Menu "MP"
	SubMenu "Dispersion"
		"Calculate energy dispersion", /Q, MP_CalcDisp()
		"Show energy dispersion", /Q, MP_ShowDisp()
		"Help", /Q, MP_DisplayDispersionHelp()
	End
End

/////////////////////////////////////////////////////////////////////

Function MP_CalcDisp()

Variable dim=50,  crop=0
String foldername="NewFolderName", strcrop="No"

Variable refNum
Open/D/R/MULT=1/M="Select all the images to load"/F="Image Files:.*;" refNum
if(stringmatch(S_fileName,""))
	Abort "Procedure aborted."
else
	String imageslist=S_fileName
endif

Variable tot=ItemsInList(imageslist, "\r")

Prompt dim, "Real space full image size (nm)"
Prompt foldername, "Folder name"
Prompt strcrop, "Select a region?", popup, "Yes;No"
DoPrompt/HELP="" "Insert", dim, foldername, strcrop
if(V_flag)
	Abort "Procedure aborted by user."
endif
// Select if crop
if(stringmatch(strcrop,"Yes"))
	crop=1
endif

String strName
Variable num=0, en=250
Variable i=0, half, pix, t
Variable thick=1
Variable left, right, top, bottom, flag=0

NewDataFolder/O/S root:$(foldername)
Make/O/N=(tot) energies

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
for(num=0;num<tot;num+=1)

//Load image
SetDataFolder root:$(foldername)
ImageLoad/O/Q StringFromList(num, imageslist, "\r")
strName=S_FileName
NewDataFolder/O/S root:$(foldername):$(strName)
Duplicate/O root:$(foldername):$(strName) image_real
KillWaves/Z root:$(foldername):$(strName)
Prompt en, "Energy (meV)"
DoPrompt/HELP="" "Insert energy for "+strName, en
if(V_flag)
	Abort "Procedure aborted by user."
endif
SetDataFolder root:$(foldername)
energies[num]=en
SetDataFolder root:$(foldername):$(strName)

//Convert image to grayscale
if (DimSize(image_real,2)==3)
	ImageTransform/O rgb2gray image_real
endif

//Crop real image

if(crop)
	if (num==0)
		DoWindow/K graph_for_user
		NewImage image_real
		DoWindow/C graph_for_user
		
		//Pause for user
		DoWindow/K panel_for_user
		NewPanel /K=1 /W=(0,0,400,80) as "Crop region"
		DoWindow/C panel_for_user					// Set to an unlikely name
		AutoPositionWindow/E/M=1/R=graph_for_user			// Put panel near the graph
		DrawText 21,20,"Select the area of interest (no selection equals no crop)"
		Button ButtonEnd, pos={200,40}, size={92,20}, title="Continue"
		Button ButtonEnd, proc=ButtonEnd
		PauseForUser panel_for_user, graph_for_user
		
		GetMarquee/K/W=graph_for_user left top
		left=round(V_left)
		right=left+round((V_right-V_left+V_bottom-V_top)/2)
		top=round(V_top)
		bottom=top+round((V_right-V_left+V_bottom-V_top)/2)
		flag=V_flag
		DoWindow/K graph_for_user
	endif
	if(flag)
		dim*=(right-left)/DimSize(image_real,0)
		DeletePoints/M=0 right,inf, image_real
		DeletePoints/M=0 0,left, image_real
		DeletePoints/M=1 bottom,inf, image_real
		DeletePoints/M=1 0,top, image_real
	else
		DoAlert 0, "No crop."
	endif
endif

//Even and equal number of rows and columns
if (DimSize(image_real,0) & 1)
	DeletePoints 0,1, image_real
endif
Variable diff=DimSize(image_real,0)-DimSize(image_real,1)
if (diff>0)
	DeletePoints/M=0 0,diff, image_real
else
	DeletePoints/M=1 0,diff, image_real
endif
//Calculate FFT
Wave image
FFT/DEST=image/OUT=3 image_real

//Create variables
pix=DimSize(image,1)
half=pix/2
t=0.5*pi/pix

SetScale x 0, half, image
SetScale y 0, pix, image

//Create waves
Make/O/N=(2*pix) xp=0, yp=0, line_temp=0
Make/O/N=(2*pix, 2*pix) profile=0
SetScale x 0, half, xp, yp
SetScale x 0, (pi*pix/dim), profile, line_temp // equivalent to (2*pi/size in nm), q in nm-1
SetScale y 0, 360, profile // angle in degrees

//Create waves
xp=0
yp=0
line_temp=0
profile=0

//Uncomment to see the profile in real time (DoUpdate in the cycle also):
//Display/K=1;DelayUpdate
//AppendImage image;DelayUpdate
//AppendToGraph yp vs xp
//

i=0
do
	xp=0.5+(x-1)*sin(i*t)
	yp=half-0.5+(x-1)*cos(i*t)
	//DoUpdate //To see profile in real time (slower!)
	ImageLineProfile srcWave=image, xWave=xp, yWave=yp, width=thick
	Wave W_ImageLineProfile, W_LineProfileX, W_LineProfileY
	profile[][i]=W_ImageLineprofile[p]
	line_temp+=W_ImageLineprofile
	i+=1
while(i<(2*pix))

//Calculate average line profile waves
line_temp/=(2*pix)
Duplicate/O line_temp, realx
realx=2*pi/x
//

/////////////////////SLICE//////////////////////

if(num==0)
	//Create energy wave
	SetDataFolder root:$(foldername)
	Make/O/N=(DimSize(profile,0), DimSize(profile,1), tot) energy
	SetScale/P x DimOffset(profile, 0), DimDelta(profile, 0), energy
	SetScale/P y DimOffset(profile, 1), DimDelta(profile, 1), energy
endif
SetDataFolder root:$(foldername):$(strName)

//Slice
Duplicate/O profile, temp
MoveWave temp root:$(foldername):temp
SetDataFolder root:$(foldername)
energy[][][num]=temp[p][q]
KillWaves/Z temp
SetDataFolder root:$(foldername):$(strName)

//////////////////Rename and display waves//////////////////

Duplicate/O image_real, $(strName+"_img")
SetScale x 0, dim, $(strName+"_img")
SetScale y 0, dim, $(strName+"_img")
//DoWindow/K $(strName+"_img")
//Display/K=1 as strName+"_img";DelayUpdate
//AppendImage $(strName+"_img");DelayUpdate
//Label left "nm";DelayUpdate
//Label bottom "nm";DelayUpdate
//ShowInfo

Duplicate/O profile, $(strName+"_ang")
SetScale x 0, (pi*pix/dim), $(strName+"_ang")
SetScale y 0, 360, $(strName+"_ang")
//DoWindow/K $(strName+"_ang")
//Display/K=1 as strName+"_ang";DelayUpdate
//AppendImage $(strName+"_ang");DelayUpdate
//Label left "Angle (deg)";DelayUpdate
//Label bottom "q (2pi/x) (nm-1)";DelayUpdate
//ShowInfo

Duplicate/O line_temp, $(strName+"_prK")
SetScale x 0, (pi*pix/dim), $(strName+"_prK")
//DoWindow/K $(strName+"_prK")
//Display/K=1 as strName+"_prK";DelayUpdate
//AppendToGraph $(strName+"_prK");DelayUpdate
//Label left "Intensity";DelayUpdate
//Label bottom "q (2pi/x) (nm-1)";DelayUpdate
//ShowInfo

Duplicate/O realx, $(strName+"_Rx")
//DoWindow/K $(strName+"_prR")
//Display/K=1 as strName+"_prR";DelayUpdate
//AppendToGraph $(strName+"_prK") vs $(strName+"_Rx");DelayUpdate
//ModifyGraph log(bottom)=1;DelayUpdate
//SetAxis/R bottom dim,*
//SetAxis/A/R bottom;DelayUpdate
//Label left "Intensity";DelayUpdate
//Label bottom "Distance (nm)";DelayUpdate
//ShowInfo
//

//Kill all other waves
KillWaves/Z image, image_real, W_ImageLineProfile, W_LineProfileX, W_LineProfileY
KillWaves/Z line_temp, yp, xp, profile, realx, temp

endfor
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

SetDataFolder root:$(foldername)

//Sort waves
Duplicate/O energies, index
MakeIndex/R energies, index
IndexSort index, energies

Duplicate/O energy energy_tmp
i=0
do
	energy_tmp[][][i]=energy[p][q][index(i)]
	i+=1
while(i<DimSize(energy, 2))

Duplicate/O energy_tmp, energy
KillWaves/Z energy_tmp

Make/O/N=(DimSize(energies,0)+1) energyx

//Set x energy scale
energyx[0]=energies[0]+(energies[0]-energies[1])/2
i=1
do
	energyx[i]=(energies[i-1]+energies[i])/2
	i+=1
while(i<DimSize(energies,0))
energyx[(DimSize(energyx,0)-1)]=energies[(DimSize(energies,0)-1)]+(energies[(DimSize(energies,0)-1)]-energies[(DimSize(energies,0)-2)])/2

KillWaves/Z temp, index

//Display cuts of energy wave
MP_ShowDisp(energy=energy)

Return 0

End

/////////////////////////////////////////////////////////////////////////////////////

Function MP_ShowDisp([energy])

	Wave energy
	
	NewDataFolder/O root:Packages
	NewDataFolder/O root:Packages:MProcedures
	Variable/G root:Packages:MProcedures:angle=90
	Variable/G root:Packages:MProcedures:range=90
	Variable/G root:Packages:MProcedures:contrast=100000
	NVAR angle=root:Packages:MProcedures:angle
	NVAR range=root:Packages:MProcedures:range
	NVAR contrast=root:Packages:MProcedures:contrast
	Wave energyx
	
	if(ParamIsDefault(energy))
		Wave energy=MP_WaveDialog()
	endif
	
	Make/O/N=(DimSize(energy,0),DimSize(energy,2)) disp
	MP_ShowDisp_Reduce(disp, energy)
	SetScale/P x DimOffset(energy, 0), DimDelta(energy, 0), disp
	DoWindow/K graph_for_user
	Display/K=1;DelayUpdate
	DoWindow/C graph_for_user;DelayUpdate
	AppendImage disp vs {*,energyx};DelayUpdate
	Label left "Energy (meV)";DelayUpdate
	Label bottom "q (2pi/x) (nm-1)";DelayUpdate
	
	//Pause for user
	DoWindow/K panel_for_user
	NewPanel /K=1 /W=(0,0,500,200) as "Energy dispersion"
	DoWindow/C panel_for_user					// Set to an unlikely name
	AutoPositionWindow/E/M=0/R=graph_for_user			// Put panel near the graph
	DrawText 21,20, "Integration parameters"
	SetVariable angle, pos={22,40}, size={150,15}, title="Angle:", limits={0,180,1}, value=angle
	Slider angleS, pos={180,40}, size={150,15}, limits={0,180,1}, vert=0, variable=angle
	SetVariable range, pos={22,80}, size={150,15}, title="+-", limits={0,90,1}, value=range
	Slider rangeS, pos={180,80}, size={150,15}, limits={0,90,1}, vert=0, variable=range
	SetVariable contrast, pos={22,150}, size={150,15}, title="Contrast:", limits={0,1000000,1}, value=contrast
	Slider contrastS, pos={180,150}, size={150,15}, limits={0,1000000,1}, vert=0, variable=contrast
	Button ButtonUpd, pos={380,30}, size={92,20}, title="Update"
	Button ButtonUpd, proc=MP_ShowDisp_ButtonUpd
	Button ButtonChe, pos={380,60}, size={92,20}, title="Checkpoint"
	Button ButtonZoo, proc=MP_ShowDisp_ButtonZoo
	Button ButtonZoo, pos={380,110}, size={92,20}, title="Reset zoom"
	Button ButtonChe, proc=MP_ShowDisp_ButtonChe
	Button ButtonEnd, pos={380,160}, size={92,20}, title="Done"
	Button ButtonEnd, proc=MP_ShowDisp_ButtonEnd
	MP_ShowDisp_ButtonUpd("")
	PauseForUser panel_for_user, graph_for_user
	
	DoWindow/K graph_for_user
	KillWaves/Z disp
	
	Return 0
	
End

Function MP_ShowDisp_ButtonUpd(ctrlName) : ButtonControl
	String ctrlName
	Wave energy, disp
	NVAR contrast=root:Packages:MProcedures:contrast
	MP_ShowDisp_Reduce(disp, energy)
	ModifyImage disp ctab= {*,contrast,Grays,0}
End

Function MP_ShowDisp_Reduce(disp, energy)
	Wave energy, disp
	NVAR angle=root:Packages:MProcedures:angle, range=root:Packages:MProcedures:range
	Variable i
	disp=0
	for(i=angle-range;i<angle+range;i+=1)
		disp+=energy[p](i)[q]
	endfor
	disp/=2*range
End

Function MP_ShowDisp_ButtonChe(ctrlName) : ButtonControl
	String ctrlName
	Wave disp, energy, energyx
	NVAR angle=root:Packages:MProcedures:angle
	NVAR range=root:Packages:MProcedures:range
	NVAR contrast=root:Packages:MProcedures:contrast
	MP_ShowDisp_Reduce(disp, energy)
	Duplicate/O disp $("Dispersion_"+num2str(angle)+"_"+num2str(range))
	SetScale/P x DimOffset(energy, 0), DimDelta(energy, 0), $("Dispersion_"+num2str(angle)+"_"+num2str(range))
	Display/K=1
	AppendImage $("Dispersion_"+num2str(angle)+"_"+num2str(range)) vs {*,energyx};DelayUpdate
	AutoPositionWindow/E/M=0/R=panel_for_user;DelayUpdate
	Label left "Energy (meV)";DelayUpdate
	Label bottom "q (2pi/x) (nm-1)";DelayUpdate
	ModifyImage $("Dispersion_"+num2str(angle)+"_"+num2str(range)) ctab= {*,contrast,Grays,0};DelayUpdate
End

Function MP_ShowDisp_ButtonZoo(ctrlName) : ButtonControl
	String ctrlName
	SetAxis/A/W=graph_for_user
End

Function MP_ShowDisp_ButtonEnd(ctrlName) : ButtonControl
	String ctrlName
	DoWindow/K panel_for_user
End

Function/WAVE MP_WaveDialog()
	String wname=""
	Prompt wname, "3D wave", popup, WaveList("*",";","DIMS:3")
	DoPrompt "Select a wave in the current data folder", wname
	Return $wname
End

/////////////////////////////////////////////////////////////////////////////////////

Function MP_DisplayDispersionHelp()
	DoWindow/K DispersionHelpNB
	NewNotebook/K=1/N=DispersionHelpNB/F=0/OPTS=3 as "Dispersion Help"
	String helpstr=ProcedureText("MP_DispersionHelp")
	helpstr=RemoveListItem(0, helpstr, "\r")
	helpstr=RemoveListItem(ItemsInList(helpstr, "\r")-1, helpstr, "\r")
	helpstr=ReplaceString("// ", helpstr, "")
	Notebook DispersionHelpNB, text=helpstr
End