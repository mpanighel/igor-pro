#pragma rtGlobals=3		// Use modern global access method and strict wave access.

//
// Read MDrift>Help
//

Function MDriftHelp()
// MDrift
// by Mirko Panighel (mirkopanighel {at} gmail {dot} com)
// Version 15.03.30

// FFT drift correction

// Help:

// To calculate drift:
// 	1. Do "FFTpnts" for Forward "F" and Backward "B" respectively
// 	2. Do "FFTcalc"

// FFTpnts()
// 	Calculate ax, ay, bx and by and store it in coef_F or coef_B
// 	Use:
// 	Open the FFT image and put CursorA on the first spot and CursorB on the second spot, the call
// 	FFTpnts(image wave, size in nm, "F" or "B", order of the points [default 1])

// FFTcalc()
// 	Calculate scaling and shear starting from coef_F and coef_B
// 	Use:
// 	FFTcalc(coef_F, coef_B, "Description note of the file to be printed in history")

// FFTstats()
// 	Just calculate A, B, alpha and beta of the FFT
// 	Open the FFT image and put CursorA on the first spot and CursorB on the second spot, the call

// FFTpnts(image wave, size in nm, order of the points [default 1])

// Drift()
// 	Calculates shear and drift from FFt parameters (alternative to FFTpnts + FFTcalc)

// CalcDrift()
// 	Embedded function, actually calculates shear and drift from FFt parameters
End

Menu "MDrift"
	"Help", /Q, DisplayMDriftHelp()
End

Function FFTpnts(image, size, fb, scaling)
	
	Wave image
	Variable size
	String fb
	Variable scaling
	
	Variable deltaA=5
	Variable deltaB=5
	
	if(DimSize(image,2)!=0)
		ImageTransform/O rgb2gray image
	endif
	
	Variable res=DimSize(image,0)
	Wave W_coef
	
	CurveFit/NTHR=0/Q Gauss2D  image[pcsr(A)-deltaA,pcsr(A)+deltaA][qcsr(A)-deltaA,qcsr(A)+deltaA] /D 
	Duplicate/O W_coef coefA1
	CurveFit/NTHR=0/Q Gauss2D  image[pcsr(B)-deltaB,pcsr(B)+deltaB][qcsr(B)-deltaB,qcsr(B)+deltaB] /D 
	Duplicate/O W_coef coefB1
	CurveFit/NTHR=0/Q Gauss2D  image[(res-pcsr(A))-deltaA,(res-pcsr(A))+deltaA][(res-qcsr(A))-deltaA,(res-qcsr(A))+deltaA] /D 
	Duplicate/O W_coef coefA2
	CurveFit/NTHR=0/Q Gauss2D  image[(res-pcsr(B))-deltaB,(res-pcsr(B))+deltaB][(res-qcsr(B))-deltaB,(res-qcsr(B))+deltaB] /D 
	Duplicate/O W_coef coefB2
	
	Make/N=4/O coef
	
	coef[0]=abs(coefA1[2]-coefA2[2])*0.5/size/scaling //ax
	coef[1]=abs(coefA1[4]-coefA2[4])*0.5/size/scaling //ay
	coef[2]=abs(coefB1[2]-coefB2[2])*0.5/size/scaling //bx
	coef[3]=abs(coefB1[4]-coefB2[4])*0.5/size/scaling //by
	
	Duplicate/O coef $"coef"+"_"+fb
	Print "coef"+"_"+fb+" saved."
	KillWaves/Z coef, coefA1, coefA2, coefB1, coefB2
	
End

Function FFTstats(image, size,scaling)
	
	Wave image
	Variable size
	Variable scaling
	
	Variable deltaA=5
	Variable deltaB=5
	
	if(DimSize(image,2)!=0)
		ImageTransform/O rgb2gray image
	endif
	
	Variable res=DimSize(image,0)
	Wave W_coef
	
	CurveFit/NTHR=0/Q Gauss2D  image[pcsr(A)-deltaA,pcsr(A)+deltaA][qcsr(A)-deltaA,qcsr(A)+deltaA] /D 
	Duplicate/O W_coef coefA1
	CurveFit/NTHR=0/Q Gauss2D  image[pcsr(B)-deltaB,pcsr(B)+deltaB][qcsr(B)-deltaB,qcsr(B)+deltaB] /D 
	Duplicate/O W_coef coefB1
	CurveFit/NTHR=0/Q Gauss2D  image[(res-pcsr(A))-deltaA,(res-pcsr(A))+deltaA][(res-qcsr(A))-deltaA,(res-qcsr(A))+deltaA] /D 
	Duplicate/O W_coef coefA2
	CurveFit/NTHR=0/Q Gauss2D  image[(res-pcsr(B))-deltaB,(res-pcsr(B))+deltaB][(res-qcsr(B))-deltaB,(res-qcsr(B))+deltaB] /D 
	Duplicate/O W_coef coefB2
		
	Variable ax=abs(coefA1[2]-coefA2[2])*0.5/size/scaling //ax
	Variable ay=abs(coefA1[4]-coefA2[4])*0.5/size/scaling //ay
	Variable bx=abs(coefB1[2]-coefB2[2])*0.5/size/scaling //bx
	Variable by=abs(coefB1[4]-coefB2[4])*0.5/size/scaling //by
	
	Print "a=", sqrt(ax^2+ay^2), "A=", 1/sqrt(ax^2+ay^2)
	Print "b=", sqrt(bx^2+by^2), "B=", 1/sqrt(bx^2+by^2)
	Print "alpha=", atan(ay/ax)*180/pi
	Print "beta=", atan(by/bx)*180/pi
	
	KillWaves/Z coefA1, coefA2, coefB1, coefB2
	
End

Function FFTcalc(coef_F, coef_B, filename)
	
	Wave coef_F, coef_B
	String filename
	Print coef_F, coef_B, filename
	CalcDrift(coef_F[0],coef_F[1],coef_F[2],coef_F[3],coef_B[0],coef_B[1],coef_B[2],coef_B[3])
	
End

Function CalcDrift(axf,ayf,bxf,byf,axb,ayb,bxb,byb)
	
	Variable axf,ayf,bxf,byf
	Variable axb,ayb,bxb,byb
	
	Variable aym=(ayf+ayb)/2
	Variable bym=(byf+byb)/2
	
	Make/O/N=(4,4) mA
	Make/O/N=4 mB
	
	mA[0][0]=aym
	mA[0][1]=-bxf
	mA[0][2]=-axf
	mA[0][3]=-bym
	
	mA[1][0]=aym
	mA[1][1]=bxb
	mA[1][2]=axb
	mA[1][3]=-bym
	
	mA[2][0]=bym
	mA[2][1]=-axf
	mA[2][2]=bxf
	mA[2][3]=aym
	
	mA[3][0]=bym
	mA[3][1]=axb
	mA[3][2]=-bxb
	mA[3][3]=aym
	
	mB[0]=aym-bxf
	mB[1]=bxb-aym
	mB[2]=bym-axf
	mB[3]=axb-bym
	
	MatrixOP/O mX=Inv(mA) x mB
	
	Variable z=mX[0]
	Variable s=mX[1]
	Variable tgphi=mX[2]
	Variable tgtheta=mX[3]
	
	Print "z=",z
	Print "s=",s
	Print "phi=",atan(tgphi)*180/pi,"tgphi=",tgphi
	Print "theta=",atan(tgtheta)*180/pi,"tgtheta=",tgtheta
	
	Variable ax= (axf*(1-s)+aym*tgtheta)/((1-z)*(1-s)-tgphi*tgtheta)
	//Print "ax=", ax, (axb*(1-s)-aym*tgtheta)/((1+z)*(1-s)+tgphi*tgtheta)
	Variable ay= (aym*(1-z)+axf*tgphi)/((1-s)*(1-z)-tgphi*tgtheta)
	//Print "ay=", ay, (aym*(1+z)+axb*tgphi)/((1-s)*(1+z)+tgphi*tgtheta)
	Variable bx= (bxf*(1-s)-bym*tgtheta)/((1-z)*(1-s)-tgphi*tgtheta
	//Print "bx=", bx, (bxb*(1-s)+bym*tgtheta)/((1+z)*(1-s)+tgphi*tgtheta
	Variable by= (bym*(1-z)-bxf*tgphi)/((1-s)*(1-z)-tgphi*tgtheta)
	//Print "by=", by, (bym*(1+z)-bxb*tgphi)/((1-s)*(1+z)+tgphi*tgtheta)
	
	Variable a=sqrt(ax^2+ay^2)
	Variable b=sqrt(bx^2+by^2)
	//Print "a=", a, "b=", b
	
	Variable Areal=1/a
	Variable Breal=1/b
	Print "A=", Areal, "B=", Breal
	
	Variable alpha=atan(ay/ax)*180/pi
	Variable betha=atan(by/bx)*180/pi
	Print "Alpha=", alpha, "Beta=", betha
	
End

Function Drift()
	
	Variable af=8.533, alphaf=37.3, bf=9.884, betaf=117.1
	Variable ab=8.521, alphab=37.5, bb=9.856, betab=117
	String filename="Cu100_01"
	
	Prompt filename "FileName"
	Prompt af, "af"
	Prompt alphaf, "alphaf"
	Prompt bf, "bf"
	Prompt betaf, "betaf"
	
	Prompt ab, "ab"
	Prompt alphab, "alphab"
	Prompt bb, "bb"
	Prompt betab, "betab"
	
	DoPrompt/HELP="" "forward and backward", af, alphaf, bf, betaf, ab, alphab, bb, betab, filename
	
	Print filename
	Print "Input:", "af=",af, "alphaf=",alphaf, "bf=",bf, "betaf=",betaf, "ab=",ab, "alphab=",alphab, "bb=",bb, "betab=",betab
	
	if(alphaf>90)
		alphaf=180-alphaf
	endif
	if(betaf>90)
		betaf=180-betaf
	endif
	if(alphab>90)
		alphab=180-alphab
	endif
	if(betab>90)
		betab=180-betab
	endif
	
	Variable axf,ayf,bxf,byf
	Variable axb,ayb,bxb,byb
	
	axf=0.5*af*cos(alphaf*pi/180)
	ayf=0.5*af*sin(alphaf*pi/180)
	bxf=0.5*bf*cos(betaf*pi/180)
	byf=0.5*bf*sin(betaf*pi/180)
	
	axb=0.5*ab*cos(alphab*pi/180)
	ayb=0.5*ab*sin(alphab*pi/180)
	bxb=0.5*bb*cos(betab*pi/180)
	byb=0.5*bb*sin(betab*pi/180)
	
	CalcDrift(axf,ayf,bxf,byf,axb,ayb,bxb,byb)
	
End

Function DisplayMDriftHelp()
	DoWindow/K MDriftHelpNB
	NewNotebook/K=1/N=MDriftHelpNB/F=0/OPTS=3 as "MDrift Help"
	String helpstr=ProcedureText("MDriftHelp")
	helpstr=RemoveListItem(0, helpstr, "\r")
	helpstr=RemoveListItem(ItemsInList(helpstr, "\r")-1, helpstr, "\r")
	helpstr=ReplaceString("// ", helpstr, "")
	Notebook MDriftHelpNB, text=helpstr
End