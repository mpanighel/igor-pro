//
// Read MP>FitFunc>Help
//

#pragma rtGlobals=3		// Use modern global access method and strict wave access.
#pragma version=1.0

Function MP_FitFuncHelp()
// FitFunc
// Version 1.0
// by Mirko Panighel (mirkopanighel {at} gmail {dot} com)
// _____________________________________________

// Various fitting functions

// Use:
// All functions are accessible in the "Analysis>Curve fitting" dialog or in the "Multipeak Fitting 2" package

// Fitting functions:

// MP_SplitGauss
// 		Splitted Gaussian
// 		f(x) =	y0 + m*x + G*exp(-((x-x0)/GwidthL)^2)	x<x0
// 				y0 + m*x + G*exp(-((x-x0)/GwidthR)^2)	x>=x0
// 		w=[y0, m, x0, G, GwidthL, GwidthR]
//
// MP_ExpModGauss
// 		Exponential Modified Gaussian (EMF)
// 		f(x) = y0 + m*x + G*Gwidth*sqrt(2*pi)*exp(Gwidth^2/2/tau^2-(x-x0)/tau)*(tau/abs(tau)+erf((x-x0)/sqrt(2)/Gwidth - Gwidth/sqrt(2)/tau))/(2*tau)
// 		w=[y0, m, x0, G, Gwidth, tau]
//
// MP_SplitLor
// 		Splitted Lorentzian
// 		f(x) =	y0 + m*x + L/(1+((x-x0)/LwidthL)^2)	x<x0
// 				y0 + m*x + L/(1+((x-x0)/LwidthR)^2)	x>=x0
// 		w=[y0, m, x0, L, LwidthL, LwidthR]
// 
// MP_PseudoVoigt
// 		PseudoVoigt linear combination of Gaussian and Lorentzian
// 		f(x) = y0 + m*x + A*(shape/(1+4*((x-x0)/hwhm)^2) + (1-shape)*sqrt(ln(2)*pi)*exp(-(4*ln(2)*((x-x0)/hwhm)^2)))
// 		w=[y0, m, x0, A, hwhm, shape]
// 
// MP_PseudoVoigtAsym
// 		PseudoVoigt sum of Gaussian and Lorentzian with FWHM replaced by a sigmoidal function (Surf. Interface Anal. 2014, 46, 505�511)
//		(also present in  "Multipeak Fitting 2")
//		Note: when fitting maybe be convenient to constrain "asym" and "xs" one at time.
// 		f(x) = y0 + m*x + A*(shape/(1+4*((x-x0)/wx)^2) + (1-shape)*exp(-(4*ln(2)*((x-x0)/wx)^2)))
//			wx=fwhm/(1+exp(-asym*(x-x0-xs)))
// 		w=[y0, m, x0, A, fwhm, shape, asym, xs]
//
// MP_Doniach-Sunjic
// 		Doniach-Sunjic
//		(also present in  "Multipeak Fitting 2")
// 		f(x) =	y0 + m*x + A*(asym*pi/2 + (1-asym)*atan((x-x0)/width))/(width+(x-x0)^2)
// 		w=[y0, m, x0, A, width, asym]
//
End

//MENUS//////////////////////////////////////////////////////////////////////
Menu "MP"
	SubMenu "FitFunc"
		"Help", /Q, MP_DisplayFitFuncHelp()
	End
End

// ***************************
// SPLITTED GAUSSIAN
// ***************************

// SplitGauss for Curve fitting dialog:
Function MP_SplitGauss(w,x) : FitFunc
	Wave w
	Variable x
	
	//CurveFitDialog/ These comments were created by the Curve Fitting dialog. Altering them will
	//CurveFitDialog/ make the function less convenient to work with in the Curve Fitting dialog.
	//CurveFitDialog/ Equation:
	//CurveFitDialog/ f(x) = y0 + m*x + G*exp(-((x-x0)/Gwidth)^2)
	//CurveFitDialog/ End of Equation
	//CurveFitDialog/ Independent Variables 1
	//CurveFitDialog/ x
	//CurveFitDialog/ Coefficients 6
	//CurveFitDialog/ w[0] = y0
	//CurveFitDialog/ w[1] = m
	//CurveFitDialog/ w[2] = x0
	//CurveFitDialog/ w[3] = G
	//CurveFitDialog/ w[4] = GwidthL
	//CurveFitDialog/ w[5] = GwidthR

	if(x<w[2])
		Return w[0] + w[1]*x + w[3]*exp(-((x-w[2])/w[4])^2)
	else
		Return w[0] + w[1]*x + w[3]*exp(-((x-w[2])/w[5])^2)
	endif
	
End

// *****************************************************
// EXPONENTIAL MODIFIED GAUSSIAN (EMF)
// *****************************************************

// ExpModGauss for Curve fitting dialog:
Function MP_ExpModGaussEMF(w,x) : FitFunc
	Wave w
	Variable x

	//CurveFitDialog/ These comments were created by the Curve Fitting dialog. Altering them will
	//CurveFitDialog/ make the function less convenient to work with in the Curve Fitting dialog.
	//CurveFitDialog/ Equation:
	//CurveFitDialog/ f(x) = y0 + m*x + G*Gwidth*sqrt(2*pi)*exp(Gwidth^2/2/tau^2-(x-x0)/tau)*(tau/abs(tau)+erf((x-x0)/sqrt(2)/Gwidth - Gwidth/sqrt(2)/tau))/(2*tau)
	//CurveFitDialog/ End of Equation
	//CurveFitDialog/ Independent Variables 1
	//CurveFitDialog/ x
	//CurveFitDialog/ Coefficients 6
	//CurveFitDialog/ w[0] = y0
	//CurveFitDialog/ w[1] = m
	//CurveFitDialog/ w[2] = x0
	//CurveFitDialog/ w[3] = G
	//CurveFitDialog/ w[4] = Gwidth
	//CurveFitDialog/ w[5] = tau
	
	Return  w[0] + w[1]*x + w[3]*w[4]*sqrt(2*pi)*exp(w[4]^2/2/w[5]^2-(x-w[2])/w[5])*(w[5]/abs(w[5])+erf((x-w[2])/sqrt(2)/w[4] - w[4]/sqrt(2)/w[5]))/(2*w[5])
	
End

// ***************************
// SPLITTED LORENTIAN
// ***************************

// SplitLor for Curve fitting dialog:
Function MP_SplitLor(w,x) : FitFunc
	Wave w
	Variable x

	//CurveFitDialog/ These comments were created by the Curve Fitting dialog. Altering them will
	//CurveFitDialog/ make the function less convenient to work with in the Curve Fitting dialog.
	//CurveFitDialog/ Equation:
	//CurveFitDialog/ y0 + m*x + L/(1+((x-x0)/LwidthL)^2)
	//CurveFitDialog/ End of Equation
	//CurveFitDialog/ Independent Variables 1
	//CurveFitDialog/ x
	//CurveFitDialog/ Coefficients 5
	//CurveFitDialog/ w[0] = y0
	//CurveFitDialog/ w[1] = m
	//CurveFitDialog/ w[2] = x0
	//CurveFitDialog/ w[3] = L
	//CurveFitDialog/ w[4] = LwidthL
	//CurveFitDialog/ w[5] = LwidthR
	
	if(x<w[2])
		Return w[0] + w[1]*x + w[3]/(1+((x-w[2])/w[4])^2)
	else
		Return w[0] + w[1]*x + w[3]/(1+((x-w[2])/w[5])^2)
	endif
	
End

// ********************
// PSEUDO VOIGT
// ********************

// PseudoVoigt for Curve fitting dialog:
Function MP_PseudoVoigt(w,x) : FitFunc
	Wave w
	Variable x

	//CurveFitDialog/ These comments were created by the Curve Fitting dialog. Altering them will
	//CurveFitDialog/ make the function less convenient to work with in the Curve Fitting dialog.
	//CurveFitDialog/ Equation:
	//CurveFitDialog/ f(x) = y0 + m*x + A*(shape/(1+4*((x-x0)/hwhm)^2) + (1-shape)*sqrt(ln(2)*pi)*exp(-(4*ln(2)*((x-x0)/hwhm)^2)))
	//CurveFitDialog/ End of Equation
	//CurveFitDialog/ Independent Variables 1
	//CurveFitDialog/ x
	//CurveFitDialog/ Coefficients 6
	//CurveFitDialog/ w[0] = y0
	//CurveFitDialog/ w[1] = m
	//CurveFitDialog/ w[2] = x0
	//CurveFitDialog/ w[3] = A
	//CurveFitDialog/ w[4] = hwhm
	//CurveFitDialog/ w[5] = shape

	Return w[0] + w[1]*x + w[3]*(w[5]/(1+4*((x-w[2])/w[4])^2) + (1-w[5])*sqrt(ln(2)*pi)*exp(-(4*ln(2)*((x-w[2])/w[4])^2)))
	//Return w[0] + w[1]*x + w[3]*(w[5]/sqrt(pi)/(0.5+8*((x-w[2])/w[4])^2) + (1-w[5])*2*sqrt(ln(2))*exp(-4*ln(2)*((x-w[2])/w[4])^2))

End

// ****************************
// PSEUDO VOIGT ASYM
// ****************************

// PseudoVoigtAsym for Curve fitting dialog:
Function MP_PseudoVoigtAsym(w,x) : FitFunc
	Wave w
	Variable x
	
	//CurveFitDialog/ These comments were created by the Curve Fitting dialog. Altering them will
	//CurveFitDialog/ make the function less convenient to work with in the Curve Fitting dialog.
	//CurveFitDialog/ Equation:
	//CurveFitDialog/ f(x) = y0 + m*x + A*(shape/(1+4*((x-x0)/fwhm)^2) + (1-shape)*sqrt(ln(2)*pi)*exp(-(4*ln(2)*((x-x0)/fwhm)^2)))
	//CurveFitDialog/ End of Equation
	//CurveFitDialog/ Independent Variables 1
	//CurveFitDialog/ x
	//CurveFitDialog/ Coefficients 8
	//CurveFitDialog/ w[0] = y0
	//CurveFitDialog/ w[1] = m
	//CurveFitDialog/ w[2] = x0
	//CurveFitDialog/ w[3] = A
	//CurveFitDialog/ w[4] = fwhm
	//CurveFitDialog/ w[5] = shape
	//CurveFitDialog/ w[6] = asym
	//CurveFitDialog/ w[7] = xs
	
	Return w[0] + w[1]*x + w[3]*(w[5]/(1+4*((x-w[2])/(w[4]/(1+exp(-w[6]*(x-w[2]-w[7])))))^2) + (1-w[5])*sqrt(ln(2)*pi)*exp(-4*ln(2)*((x-w[2])/(w[4]/(1+exp(-w[6]*(x-w[2]-w[7])))))^2))
	//Return w[0] + w[1]*x + w[3]*(w[5]/sqrt(pi)/(0.5+8*((x-w[2])/(w[4]/(1+exp(-w[6]*(x-w[7])))))^2) + (1-w[5])*2*sqrt(ln(2))*exp(-4*ln(2)*((x-w[2])/(w[4]/(1+exp(-w[6]*(x-w[7])))))^2))

End

// PseudoVoigtAsym for Multipeak Fitting 2:
Function MP_PseudoVoigtAsym_Peak(w,yw,xw)
	Wave w, yw, xw
	yw = w[1]*(w[3]/(1+4*((xw-w[0])/(w[2]/(1+exp(-w[4]*(xw-w[0]-w[5])))))^2) + (1-w[3])*sqrt(ln(2)*pi)*exp(-4*ln(2)*((xw-w[0])/(w[2]/(1+exp(-w[4]*(xw-w[0]-w[5])))))^2))
End

Function/S MP_PseudoVoigtAsym_PeakFuncInfo(InfoDesired)
	Variable InfoDesired
	String info=""
	Switch (InfoDesired)
		case PeakFuncInfo_ParamNames:
			info = "x0;A;fwhm;shape;asym;xs;"
			break;
		case PeakFuncInfo_PeakFName:
			info = "MP_PseudoVoigtAsym_Peak"
			break;
		case PeakFuncInfo_GaussConvFName:
			info = "GaussToPseudoVoigtAsymGuess"
			break;
		case PeakFuncInfo_ParameterFunc:
			info = "MP_PseudoVoigtAsym_PeakParams"
			break;
		case PeakFuncInfo_DerivedParamNames:
			info = "Location;Height;Area;FWHM;"
			break;
		default:
			break;
	endSwitch
	Return info
End

Function GaussToPseudoVoigtAsymGuess(w)
	Wave w
	Variable Glocation=w[0], Gwidth=w[1], Gheight=w[2]
	Redimension/N=6 w
	w[0]=Glocation //x0
	w[1]=Gheight //A
	w[2]=2*Gwidth //fwhm
	w[3]=0.5 //shape
	w[4]=0.1 //asym
	w[5]=0 //xs
	Return 0
End

Function MP_PseudoVoigtAsym_PeakParams(cw, sw, outWave)
	Wave cw, sw, outWave
	Variable fwhm=cw[2]/(1+exp(cw[4]*cw[5]))
	// Location
	outWave[0][0] = (cw[0]-cw[5])/(1+exp(cw[4]*cw[0]))-cw[5]
	outWave[0][1] = sqrt(sw[0][0])
	// Height
	outWave[1][0] = cw[1]
	outWave[1][1] = sqrt(sw[1][1])
	// Area
	outWave[2][0] =  cw[1]*fwhm*sqrt(pi)
	outWave[2][1] =  sqrt( (outWave[2][0]^2)*((sw[2][2]/cw[2]^2) + (sw[1][1]/cw[1]^2) + 2*sw[1][2]/(cw[1]*cw[2])) )
	// FWHM
	outWave[3][0] = fwhm
	outWave[3][1] = sqrt(sw[2][2])
End

// **********************
// DONIACH SUNJIC
// **********************

// DoniachSunjic for Curve fitting dialog:
Function MP_DoniachSunjic(w,x) : FitFunc
	Wave w
	Variable x

	//CurveFitDialog/ These comments were created by the Curve Fitting dialog. Altering them will
	//CurveFitDialog/ make the function less convenient to work with in the Curve Fitting dialog.
	//CurveFitDialog/ Equation:
	//CurveFitDialog/ f(x) =	 y0 + m*x + A*(asym*pi/2 + (1-asym)*atan((x-x0)/width))/(width+(x-x0)^2)
	//CurveFitDialog/ End of Equation
	//CurveFitDialog/ Independent Variables 1
	//CurveFitDialog/ x
	//CurveFitDialog/ Coefficients 5
	//CurveFitDialog/ w[0] = y0
	//CurveFitDialog/ w[1] = m
	//CurveFitDialog/ w[2] = x0
	//CurveFitDialog/ w[3] = A
	//CurveFitDialog/ w[4] = width
	//CurveFitDialog/ w[5] = asym
	
	Return w[0] + w[1]*x + w[3]*(w[5]*pi/2 + (1-w[5])*atan((x-w[2])/w[4]))/(w[4]+(x-w[2])^2)
	
End

// DoniachSunjic for Multipeak Fitting 2:
Function MP_DoniachSunjic_Peak(w,yw,xw)
	Wave w, yw, xw
	yw = w[1]*(w[3]*pi/2 + (1-w[3])*atan((x-w[0])/w[2]))/(w[2]+(x-w[0])^2)
End

Function/S MP_DoniachSunjic_PeakFuncInfo(InfoDesired)
	Variable InfoDesired
	String info=""
	Switch (InfoDesired)
		case PeakFuncInfo_ParamNames:
			info = "x0;A;width;asym;"
			break;
		case PeakFuncInfo_PeakFName:
			info = "MP_DoniachSunjic_Peak"
			break;
		case PeakFuncInfo_GaussConvFName:
			info = "GaussToDoniachSunjicGuess"
			break;
		case PeakFuncInfo_ParameterFunc:
			info = "MP_DoniachSunjic_PeakParams"
			break;
		case PeakFuncInfo_DerivedParamNames:
			info = "Location;Height;Area;FWHM;"
			break;
		default:
			break;
	endSwitch
	Return info
End

Function GaussToDoniachSunjicGuess(w)
	Wave w
	Variable Glocation=w[0], Gwidth=w[1], Gheight=w[2]
	Redimension/N=4 w
	w[0]=Glocation //x0
	w[1]=Gheight //A
	w[2]=2*Gwidth //fwhm
	w[3]=0.5 //asym
	Return 0
End

Function MP_DoniachSunjic_PeakParams(cw, sw, outWave)
	Wave cw, sw, outWave
	// Location
	outWave[0][0] = cw[0]
	outWave[0][1] = sqrt(sw[0][0])
	// Height
	outWave[1][0] = cw[1]
	outWave[1][1] = sqrt(sw[1][1])
	// Area
	outWave[2][0] =  cw[1]*cw[2]*sqrt(pi)
	outWave[2][1] =  sqrt( (outWave[2][0]^2)*((sw[2][2]/cw[2]^2) + (sw[1][1]/cw[1]^2) + 2*sw[1][2]/(cw[1]*cw[2])) )
	// FWHM
	outWave[3][0] = cw[2]
	outWave[3][1] = sqrt(sw[2][2])
End

//*************************
// ***** Help menu *****
Function MP_DisplayFitFuncHelp()
	DoWindow/K FitFuncHelpNB
	NewNotebook/K=1/N=FitFuncHelpNB/F=0/OPTS=3 as "FitFunc Help"
	String helpstr=ProcedureText("MP_FitFuncHelp")
	helpstr=RemoveListItem(0, helpstr, "\r")
	helpstr=RemoveListItem(ItemsInList(helpstr, "\r")-1, helpstr, "\r")
	helpstr=ReplaceString("// ", helpstr, "")
	Notebook FitFuncHelpNB, text=helpstr
End