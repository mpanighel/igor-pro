//
// Read MP>Procedures>Help
//

#pragma rtGlobals=3		// Use modern global access method and strict wave access.
#pragma version=1.0

Function MP_ProceduresHelp()
// Procedures
// Version 1.0.1
// by Mirko Panighel (mirkopanighel {at} gmail {dot} com)
// _____________________________________________

// Useful functions and extensions for Wavemetrics Igor Pro

// Use:
// All functions are accessible in the "MP>Procedures" menu, or
// by right-clicking on a trace in a graph or
// by right-clicking in a marquee (zoom) area.

// Help:

// Menu "Procedures"
// 	"Do Loop...", Loop a command where $ is the index
// 	"Load from text file", Load waves from ASCII file
// 	"Help", Show this help

// Menu "TracePopup" (appears by right-clicking on a trace in a graph)
// 	Submenu "MP Graph"
// 		"Trace list...", List all traces in the graph
// 		"Duplicate Graph", Duplicate the graph (CTRL+0, same as CTRL+D)
// 		"Copy trace...", Copy the trace
// 		"Copy all traces...", Copy all the traces in the graph (CTRL+8)
// 		"Paste traces...", Paste the copied traces (CTRL+9)
// 		SubMenu "Reorder trace"
// 			"Send back", Send to back the trace (CTRL+6)
// 			"Bring front", Bring to front the trace (CTRL+7)
// 		"Display this trace in a new graph"
// 		"Export this trace to text file..."
// 		"Export all traces to text file..."
// 	SubMenu "MP Trace..."
// 		"Copy wave full path...", Display full path of the wave in the Command Line
// 		"Copy trace name...", Display trace name in the Command Line
// 		"Rename wave..."
// 		"Area between cursors", Calculate the area underneath the trace between the two cursors
// 		SubMenu "Backup-Restore..."
// 			"Change X wave...", Replace the X wave of a trace (for XY traces)
// 			"Change Y wave...", Replace the trace or the Y wave
// 			"Backup this wave", Create a backup copy of the trace
// 			"Backup all waves", Create a backup copy for all traces in the graph
// 			"Restore this wave", Replace with the created backup copy
// 			"Restore all waves", Replace all traces with the created backup copies
// 		Submenu "Analysis"
// 			"Remove spike on Cursor[A]", Replace the cursor value with the artithmetic mean of the neighbouring points
// 			"Smooth wave", Copy and smooth the wave on the graph
// 	SubMenu "MP Offset..."
// 		SubMenu "Apply X shift..."
// 			"Cursor[A] to zero", X shift: CursorA X value is set to zero
// 			"Cursor[A] to cursor[B]", X shift: CursorA X value is set to CursorB X value
// 			"By value...", X shift by a given value (open prompt)
// 			"Apply current offset to this trace", Apply the trace offset as a shift
// 			"Apply current offset to all traces in the graph", Apply the trace offset as a shift to all waves
// 		SubMenu "Apply Y multiply..."
// 			"Cursor[A] to one", Y multiplication: CursorA Y value is set to one
// 			"Cursor[A] to cursor[B]", Y multiplication: CursorA Y value is set to CursorB Y value
// 			"By value...",  Y multiplication by a given value (open prompt)
// 			"Apply current mulOffset to this trace", Apply the trace multiply offset as a scale
// 			"Apply current mulOffset to all traces in the graph", Apply the trace multiply offset as a scale to all waves
// 		SubMenu "Apply Y shift..."
// 			"Cursor[A] to zero", Y shift: CursorA Y value is set to zero
// 			"Cursor[A] to cursor[B]", Y shift: CursorA Y value is set to CursorB Y value
// 			"By value...", Y shift by a given value (open prompt)
// 			"Apply current offset to this trace", Apply the trace offset as a shift
// 			"Apply current offset to all traces in the graph", Apply the trace offset as a shift to all waves
// 		Submenu "Fake X offset..."
// 			"Cursor[A] to zero", X offset: CursorA X value is set to zero
// 			"Cursor[A] to cursor[B]", X offset: CursorA X value is set to CursorB X value (CTRL+1)
// 		Submenu "Fake Y mulOffset..."
// 			"Cursor[A] to one", Y multiplication offset: CursorA Y value is set to one
// 			"Cursor[A] to cursor[B]", Y multiplication offset: CursorA Y value is set to CursorB Y value (CTRL+2)
// 		Submenu "Fake Y offset..."
// 			"Cursor[A] to zero", Y offset: CursorA Y value is set to zero
// 			"Cursor[A] to cursor[B]", Y offset: CursorA Y value is set to CursorB Y value
// 	SubMenu "MP Arithmetics..."
// 		Submenu "Sum waves..."
// 			"Cursor[A]+Cursor[B]", Sum CursorA and CursorB traces
// 			"ThisWave+Cursor[B]", Sum the selected trace with CursorB trace
// 		Submenu "Subtract waves..."
// 			"Cursor[A]-Cursor[B]", Subtract CursorB trace from CursorA trace
// 			"This wave-Cursor[B] wave", Subtract CursorfB trace from the selected trace
// 		Submenu "Multiply waves..."
// 			"Cursor[A]*Cursor[B]", Multiply CursorA and CursorB waves
// 			"This wave*Cursor[B] wave", Multiply the selected trace with CursorB trace
// 		Submenu "Divide waves..."
// 			"Cursor[A]_Cursor[B]", Divide CursorA trace by CursorB trace
// 			"This wave_Cursor[B] wave", Divide the selected trace by CursorfA trace

// Menu "GraphMarquee" (appears by right-clicking in a marquee-zoom area)
// 	"MP Crop last selected wave to selection", Crop the wave (only X axis)
// 	"MP Crop all waves to selection", Crop all the waves inside the marquee (only X axis)
// 
// Changelog:

// v1.0.2	Bug fixes:
// 		- Corrected error in "Calculate area"

// v1.0.1	Bug fixes:
// 		- Corrected all procedures for wave names with quotes (')
// 		- Corrected Wave Arithmetic for waves with X scaling
End

//MENUS//////////////////////////////////////////////////////////////////////
Menu "TracePopup"
SubMenu "MP"
	SubMenu "MP Graph"
		"Trace list...", /Q, MP_TraceList()
		"Duplicate Graph/0", /Q, MP_DuplicateGraph()
		"Copy trace...", /Q, MP_CopyTraces(0)
		"Copy all traces.../8", /Q, MP_CopyTraces(1)
		"Paste traces.../9", /Q, MP_PasteTraces()
		SubMenu "Reorder trace"
			"Send back/6", /Q, MP_SendBackTrace()
			"Bring front/7", /Q, MP_BringFrontTrace()
		End
		"Display this trace in a new graph", /Q, MP_DisplayWave()
		"Export this trace to text file...", /Q, MP_ExportTraces(0)
		"Export all traces to text file...", /Q, MP_ExportTraces(1)
	End
	SubMenu "MP Trace..."
		"Copy wave full path...", /Q, MP_CopyFullPath()
		"Copy trace name...", /Q, MP_TraceName()
		"Rename wave...", /Q, MP_RenameWave()
		"Area between cursors", /Q, MP_CalcArea()
		SubMenu "Backup-Restore..."
			"Change X wave...", /Q, MP_xRevert()
			"Change Y wave...", /Q, MP_yRevert()
			"Backup this wave", /Q, MP_BackupWaves(0)
			"Backup all waves", /Q, MP_BackupWaves(1)
			"Restore this wave", /Q, MP_RestoreWaves(0)
			"Restore all waves", /Q, MP_RestoreWaves(1)
		End
		Submenu "Analysis"
			"Remove spike on Cursor[A]", /Q, MP_RemoveSpike()
			"Smooth wave", /Q, MP_SmoothWave()
		End
	End
	SubMenu "MP Offset..."
		SubMenu "Apply X shift..."
			"Cursor[A] to zero", /Q, MP_xOff(1)
			"Cursor[A] to cursor[B]", /Q, MP_xOff(2)
			"By value...", /Q, MP_xOff(0)
			"Apply current offset to this trace", /Q, MP_ApplyXOffset(0)
			"Apply current offset to all traces in the graph", /Q, MP_ApplyXOffset(1)
		End
		SubMenu "Apply Y multiply..."
			"Cursor[A] to one", /Q, MP_yMul(1)
			"Cursor[A] to cursor[B]", /Q, MP_yMul(2)
			"By value...", /Q, MP_yMul(0)
			"Apply current mulOffset to this trace", /Q, MP_ApplyYMulOffset(0)
			"Apply current mulOffset to all traces in the graph", /Q, MP_ApplyYMulOffset(1)
		End
		SubMenu "Apply Y shift..."
			"Cursor[A] to zero", /Q, MP_yOff(1)
			"Cursor[A] to cursor[B]", /Q, MP_yOff(2)
			"By value...", /Q, MP_yOff(0)
			"Apply current offset to this trace", /Q, MP_ApplyYOffset(0)
			"Apply current offset to all traces in the graph", /Q, MP_ApplyYOffset(1)
		End
		Submenu "Fake X offset..."
			"Set Cursor[A] to zero", /Q, MP_SetXOffset(1)
			"Set Cursor[A] to cursor[B]/1", /Q, MP_SetXOffset(2)
		End
		Submenu "Fake Y mulOffset..."
			"Set Cursor[A] to one", /Q, MP_SetYMulOffset(1)
			"Set Cursor[A] to cursor[B]/2", /Q, MP_SetYMulOffset(2)
		End
		Submenu "Fake Y offset..."
			"Set Cursor[A] to zero", /Q, MP_SetYOffset(1)
			"Set Cursor[A] to cursor[B]", /Q, MP_SetYOffset(2)
		End
	End
	SubMenu "MP Arithmetics..."
		Submenu "Sum waves..."
			"Cursor[A]+Cursor[B]", /Q, MP_MathWaves(1, "plus")
			"ThisWave+Cursor[B]", /Q, MP_MathWaves(0, "plus")
		End
		Submenu "Subtract waves..."
			"Cursor[A]-Cursor[B]", /Q, MP_MathWaves(1, "minus")
			"This wave-Cursor[B] wave", /Q, MP_MathWaves(0, "minus")
		End
		Submenu "Multiply waves..."
			"Cursor[A]*Cursor[B]", /Q, MP_MathWaves(1, "mult")
			"This wave*Cursor[B] wave", /Q, MP_MathWaves(0, "mult")
		End
		Submenu "Divide waves..."
			"Cursor[A]_Cursor[B]", /Q, MP_MathWaves(1, "div")
			"This wave_Cursor[B] wave", /Q, MP_MathWaves(0, "div")
		End
	End
End
End

Menu "GraphMarquee"
SubMenu "MP"
	"MP Crop last selected wave to selection", /Q, MP_CropWaves(0)
	"MP Crop all waves to selection", /Q, MP_CropWaves(1)
End
End

Menu "MP"
	SubMenu "Procedures"
	"Do Loop...", /Q, MP_DoLoop()
	"Load from text file", /Q, MP_LoadTxt()
	"Help", /Q, MP_DisplayProceduresHelp()
	End
End

//FUNCTIONS///////////////////////////////////////////////////////////////

Function MP_TraceList()
	String list=TraceNameList("",";",1)
	String graph=WinName(0,1)
	String TraceList=graph+"\r"
	Variable i
	for(i=0;i<ItemsInList(list);i+=1)
		String trace=StringFromList(i,list)
		Wave wy=TraceNameToWaveRef(graph, trace)
		Wave wx=XWaveRefFromTrace(graph, trace)
		if (WaveExists(wx))
			TraceList+=GetWavesDataFolder(wy,2)+" vs "+GetWavesDataFolder(wx,2)+"\r"
		else
			TraceList+=GetWavesDataFolder(wy,2)+"\r"
		endif
	endfor
	Print TraceList
End

Function MP_DuplicateGraph()
	String winrec=WinRecreation("",0)
	winrec=winrec[strsearch(winrec, "\tSetDataFolder ",0),strlen(winrec)-10]
	winrec=RemoveFromList(ListMatch(winrec, "Window *", "\r"), winrec, "\r")
	winrec=RemoveFromList(ListMatch(winrec, "\tPauseUpdate; *", "\r"), winrec, "\r")
	winrec=ReplaceString("EndMacro", winrec, "")
	winrec=ReplaceString("fldrSav0", winrec, GetDataFolder(1))
	Variable i
	for(i=0;i<ItemsInList(winrec, "\r");i+=1)
		Execute StringFromList(i, winrec, "\r")
	endfor
	DoAlert 0, "Graph duplicated."
End

//Print wave full path in history
Function MP_CopyFullPath()
	GetLastUserMenuInfo
	String trace=S_traceName
	Wave wy=TraceNameToWaveRef("", trace)
	print GetWavesDataFolder(wy,2)
End

Function MP_TraceName()
	GetLastUserMenuInfo
	print S_traceName
End

Function MP_SendBackTrace()
	
	GetLastUserMenuInfo
	String trace=S_traceName
	String graph=S_graphname
	String tracelist=TraceNameList(WinName(0,1), ";", 1)
	
	Variable tracepos=WhichListItem(trace, tracelist)
	String traceback=StringFromList(tracepos-1,tracelist)
	if(stringmatch(traceback,""))
		Abort "Trace already on back"
	endif
	ReorderTraces $traceback,{$trace}
	Print "ReorderTraces "+traceback+",{"+trace+"}"
	
End

Function MP_BringFrontTrace()
	
	GetLastUserMenuInfo
	String trace=S_traceName
	String graph=S_graphname
	String tracelist=TraceNameList(WinName(0,1), ";", 1)
	
	Variable tracepos=WhichListItem(trace, tracelist)
	String tracefore=StringFromList(tracepos+1,tracelist)
	
	if(stringmatch(tracefore,""))
		Abort "Trace already on top"
	endif
	ReorderTraces $trace,{$tracefore}
	Print "ReorderTraces "+trace+",{"+tracefore+"}"
	
End

Function MP_CopyTraces(flag)
	Variable flag
	NewDataFolder/O root:Packages
	GetLastUserMenuInfo
	String/G root:Packages:tracelist=S_traceName
	String/G root:Packages:graph=S_graphname
	if(flag)
		String/G root:Packages:tracelist=TraceNameList(WinName(0,1), ";", 1+4)//Visible normal traces
		String/G root:Packages:graph=WinName(0,1)
	endif
	SVAR graph=root:Packages:graph
	SVAR tracelist=root:Packages:tracelist
	String/G root:Packages:winrec
	SVAR winrec=root:Packages:winrec
	//winrec=winrec[strsearch(winrec, "ModifyGraph",0),strlen(winrec)-10]
	winrec=GrepList(WinRecreation(graph, 0), "ModifyGraph", 0, "\r")
	winrec=ReplaceString("ModifyGraph", winrec, "ModifyGraph/Z")
	//winrec=ReplaceString("Legend/C/N=text0", winrec, "Legend")
	//winrec=RemoveFromList(ListMatch(winrec, "Cursor", "\r"), winrec, "\r")
	print "Copied "+num2str(ItemsInList(tracelist))+" trace(s)."
End

Function MP_PasteTraces()
	SVAR tracelist=root:Packages:tracelist
	SVAR graph=root:Packages:graph
	SVAR winrec=root:Packages:winrec
	Variable i,j
	for(i=0;i<ItemsInList(tracelist);i+=1)
		String trace=StringFromList(i,tracelist)
		Wave wy=TraceNameToWaveRef(graph, trace)
		Wave wx=XWaveRefFromTrace(graph, trace)
		
		String thistrace=RemoveFromList(ListMatch(winrec, "*)=*", "\r"), winrec, "\r")//Remove each "ModifyGraph" of specific traces
		thistrace=ReplaceString("=", thistrace, "("+trace+")=")//Set them just for the current trace
		thistrace+="\r"
		thistrace+=GrepList(winrec, "("+trace+")", 0, "\r")//Add each "ModifyGraph" of the current trace
		
		//Check instances of the current trace
		String  traceBase=trace, suffix=""
		Variable traceInstance=0
		
		if(stringmatch(trace, "*#*"))
			traceBase=trace[0, strsearch(trace, "#", Inf, 1)-1]//get base name of the trace
		endif
		
		Variable tracePos=strsearch(TraceNameList("", ";", 1), traceBase, Inf, 1)
		if(tracePos>-1)//Trace with the same name already in the graph
			if(stringmatch(TraceNameList("", ";", 1)[tracePos+strlen(traceBase)], "#"))//more than one instance with the same name
				sscanf  TraceNameList("", ";", 1)[tracePos+strlen(traceBase)+1,Inf], "%f%*s", traceInstance//get last instance
			endif
			traceInstance+=1
			suffix="#"+num2str(traceInstance)
		endif
		
		//Correct instance
		thistrace=ReplaceString("("+trace+")", thistrace, "("+traceBase+suffix+")")//Set them just for the current trace
		
		//Append traces
		if(WaveExists(wx))
			AppendToGraph wy vs wx
		else
			AppendToGraph wy
		endif
		
		//Apply ModifyGraph
		for(j=0;j<ItemsInList(thistrace, "\r");j+=1)
			Execute StringFromList(j, thistrace, "\r")
		endfor
	endfor
	
	print "Pasted "+num2str(i)+" trace(s)."
	
End

Function MP_ExportTraces(flag)
	Variable flag
	GetLastUserMenuInfo
	String tracelist=S_traceName
	if(flag)
		tracelist=TraceNameList("", ";", 1)
	endif
	Variable f1
	Variable i
	Open/D refNum
	String pathname=RemoveListItem(ItemsInList(S_filename, ":")-1, S_filename, ":")
	String filename=RemoveEnding(StringFromList(ItemsInList(S_filename, ":")-1, S_filename, ":"), ".txt")
	NewPath/O/Q pathsave, pathname
	Close 1
	for(i=0;i<ItemsInList(tracelist);i+=1)
		String trace=StringFromList(i,tracelist,";")
		Wave wy=TraceNameToWaveRef("", trace)
		Wave wx=XWaveRefFromTrace("", trace)
		if (WaveExists(wx))
			Save/O/J/M="\r\n"/W/P=pathsave wx,wy as filename+"_"+trace+".dat"
		else
			Duplicate/O wy, wx_tmp
			wx_tmp=DimOffset(wy,0)+DimDelta(wy,0)*x
			Save/O/J/M="\r\n"/W/P=pathsave wx_tmp,wy as filename+"_"+trace+".dat"
		endif
	endfor
	KillStrings/Z tracelist
	KillWaves/Z wx_tmp
	Abort num2str(i)+" files saved."
End

Function MP_BackupWaves(flag)
	Variable flag
	GetLastUserMenuInfo
	String tracelist=S_traceName
	if(flag)
		tracelist=TraceNameList("", ";", 1)
	endif
	Variable i
	for(i=0;i<ItemsInList(tracelist);i+=1)
		String trace=StringFromList(i,tracelist)
		Wave wy=TraceNameToWaveRef("", trace)
		String wy_path=GetWavesDataFolder(wy,1)
		String wy_bak=NameOfWave(wy)+"_bak"
		wy_bak=MP_CheckWaveName(wy_bak, "Backup wave name")
		Duplicate/O wy $(wy_path+"'"+wy_bak+"'")
		print "Duplicate/O "+wy_path+"'"+NameOfWave(wy)+"'"+" "+wy_path+"'"+wy_bak+"'"
	endfor
End

Function MP_RestoreWaves(flag)
	Variable flag
	GetLastUserMenuInfo
	String tracelist=S_traceName
	if(flag)
		tracelist=TraceNameList("", ";", 1)
	endif
	Variable i
	for(i=0;i<ItemsInList(tracelist);i+=1)
		String trace=StringFromList(i,tracelist)
		Wave wy=TraceNameToWaveRef("", trace)
		SetDataFolder GetWavesDataFolderDFR(wy)
		String swy_bak=GetWavesDataFolder(wy,1)+"'"+NameOfWave(wy)+"_bak'"
		if(!WaveExists($swy_bak))
			Prompt swy_bak, "Original Y wave for "+NameOfWave(wy), popup, WaveList("*", ";", "")
			DoPrompt/HELP="" "Revert wave", swy_bak
			if(V_flag)
 				Abort "Wave not reverted."
 			endif
 		endif
 		Wave wy_bak=$swy_bak
		wy=wy_bak
		print GetWavesDataFolder(wy,2)+"="+GetWavesDataFolder(wy_bak,2)
	endfor
End

Function MP_DisplayWave()
	GetLastUserMenuInfo
	String trace=S_traceName
	Wave wy=TraceNameToWaveRef("", trace)
	Wave wx=XWaveRefFromTrace("", trace)
	if (WaveExists(wx))
		Display wy vs wx
	else
		Display wy
	endif
	Legend
End

Function MP_RenameWave()
	GetLastUserMenuInfo
	String trace=S_traceName
	Wave wy=TraceNameToWaveRef("", trace)
	SetDataFolder GetWavesDataFolderDFR(wy)
	String newname="'"+NameOfWave(wy)+"'"
	Prompt newname, "New name: "
 	DoPrompt/HELP="" "Rename wave", newname
 	if(V_flag)
 		Abort "Wave not renamed."
 	endif
 	newname=MP_CheckWaveName(newname, "Rename wave")
 	if(WaveExists($newname))
 		DoAlert 0, "Name not valid or already in use."
		MP_RenameWave()
	else
		Rename wy $newname
		Print "Rename "+GetWavesDataFolder(wy,2)+" "+newname
	endif
End

Function MP_RemoveSpike()
	Wave wy=CsrWaveRef(A)
	if(!WaveExists(wy))
		Abort "Cursor[A] is not on the graph!"
	endif
	Variable before=1
	Variable after=1
	Prompt before, "Points before: "
	Prompt after, "Points after: "
 	DoPrompt/HELP="" "Remove spike", before, after
 	if(V_flag)
 		Abort "Spike not removed."
 	endif
	wy[pcsr(A)]=(wy[pcsr(A)-before]+wy[pcsr(A)+after])/2
End

Function MP_CalcArea()
	
	String dfA = GetWavesDataFolder(CsrWaveRef(A), 2)
	String dfB = GetWavesDataFolder(CsrWaveRef(B), 2)
	if (CmpStr(dfA, dfB) != 0)
		Abort "Both cursors must be on the same wave."
	endif
	
	Variable a,b
	
	Wave wy=CsrWaveRef(A)
	a=pnt2x(wy,pcsr(A))
	b=pnt2x(wy,pcsr(B))
	
	Variable bkg_area=(wy(a)+wy(b))*abs(b-a)/2
	Variable spec_area=sum(wy, a, b)*abs((b-a)/(pcsr(B)-pcsr(A)))
	
	Print NameOfWave(wy), "["+num2str(a)+","+num2str(b)+"]", "Background=", bkg_area, "Area=", spec_area-bkg_area
	
End

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Function MP_xRevert()
	GetLastUserMenuInfo
	String trace=S_traceName
	String graph=S_graphName
	Wave wx=XWaveRefFromTrace("", trace)
	
	if(!WaveExists(wx))
		Abort "The trace has no X wave. Abort."
	endif
	
	SetDataFolder GetWavesDataFolderDFR(wx)
	String wvname
	String kill="Yes"
	Prompt wvname, "Original X wave for "+NameOfWave(wx), popup, WaveList("*", ";", "")
	Prompt kill, "Kill other unused X waves: ", popup, "Yes;No"
	DoPrompt/HELP="" "Revert X wave", wvname, kill
	if(V_flag)
 		Abort "X wave not reverted."
 	endif
	Wave wx_orig=$wvname
	CheckDisplayed/A wx_orig
	if(V_flag)
		CheckDisplayed/W=$graph wx_orig
		if(V_flag)
			DoAlert 0, "X wave already original."
		else
			Abort NameOfWave(wx_orig)+" is in use in another graph. Abort."
		endif
	else
		wx=wx_orig
		KillWaves/Z wx_orig
		Rename wx $wvname
	endif
	if(stringmatch(kill,"Yes"))
		String killlist=WaveList(wvname+"*_xoff*", ";", "")
		Variable i
		for(i=0;i<ItemsInList(killlist);i+=1)
			Execute "KillWaves/Z "+StringFromList(i,killlist)
		endfor
	endif
End

Function MP_yRevert()
	GetLastUserMenuInfo
	String trace=S_traceName
	String graph=S_graphName
	Wave wy=TraceNameToWaveRef("", trace)
	SetDataFolder GetWavesDataFolderDFR(wy)
	String wvname
	String kill="Yes"
	Prompt wvname, "Original Y wave for "+NameOfWave(wy), popup, WaveList("*", ";", "")
	Prompt kill, "Kill other unused Y waves: ", popup, "Yes;No"
	DoPrompt/HELP="" "Revert Y wave", wvname, kill
	if(V_flag)
 		Abort "Y wave not reverted."
 	endif
	Wave wy_orig=$wvname
	CheckDisplayed/A wy_orig
	if(V_flag)
		CheckDisplayed/W=graph wy_orig
		if(V_flag)
			DoAlert 0, "Y wave already original."
		else
			Abort NameOfWave(wy_orig)+" is in use in another graph. Abort."
		endif
	else
		wy=wy_orig
		KillWaves/Z wy_orig
		Rename wy $wvname
	endif
	if(stringmatch(kill,"Yes"))
		String killlist=WaveList(wvname+"*_yoff*", ";", "")
		Variable i
		for(i=0;i<ItemsInList(killlist);i+=1)
			Execute "KillWaves/Z "+StringFromList(i,killlist)
		endfor
		killlist=WaveList(wvname+"*_ymul*", ";", "")
		for(i=0;i<ItemsInList(killlist);i+=1)
			Execute "KillWaves/Z "+StringFromList(i,killlist)
		endfor
	endif
End

Function MP_ApplyXOffset(flag)
	Variable flag
	DoAlert/T="Apply X offset", 1, "Really apply to wave?"
	if(!V_flag)
		Abort
	endif
	GetLastUserMenuInfo
	String list=S_traceName
	String graph=S_graphName
	if(flag)
		list=TraceNameList(WinName(0,1), ";", 1)
		graph=WinName(0,1)
	endif
	Variable i
	Variable first, last
	
	for(i=0;i<ItemsInList(list);i+=1)
		String trace=StringFromList(i,list)
		Variable xoffset=0, yoffset=0
		String info=TraceInfo(graph,trace,0)
		Variable pos=strsearch(info, "offset(x)={", 0)
		sscanf info[pos,strlen(info)], "offset(x)={%f,%f%*s", xoffset, yoffset
		if(xoffset==0)
			print trace+" has no X offset."
			continue
		endif
		
		Wave wy=TraceNameToWaveRef(graph, trace)
		Wave wx=XWaveRefFromTrace(graph, trace)
		if (WaveExists(wx))
			if(strsearch(NameOfWave(wx),"_xoff",0)==-1)
				Wave wx_xoff=$(GetWavesDataFolder(wx,1)+"'"+NameOfWave(wx)+"_xoff'")
				CheckDisplayed/A wx_xoff
				if(V_flag)
					Abort NameOfWave(wx_xoff)+" is in use in another graph. Abort."
				else
					KillWaves/Z wx_xoff
				endif
				Duplicate wx wx_orig
					wx+=xoffset
				String wxname=NameOfWave(wx)+"_xoff"
				wxname=MP_CheckWaveName(wxname, "X offset wave name")
				Rename wx $wxname
				String swx_orig=RemoveEnding(GetWavesDataFolder(wx,1)+NameOfWave(wx),"_xoff")
				swx_orig=RemoveEnding(GetWavesDataFolder(wx,1)+"'"+NameOfWave(wx)+"'","_xoff'")
				Duplicate/O wx_orig $swx_orig
				Print GetWavesDataFolder(wx,2)+"+="+num2str(xoffset)
			else
				wx+=xoffset
				Print GetWavesDataFolder(wx,2)+"+="+num2str(xoffset)
			endif
		else 
			first=DimOffset(wy,0)
			last=DimOffset(wy,0)+DimDelta(wy,0)*(numpnts(wy)-1)
			SetScale/I x (first+xoffset),(last+xoffset), wy
			Print "Setscale//I x "+num2str(first)+","+num2str(last)+", "+ GetWavesDataFolder(wy,2)
			Print "Setscale//I x "+num2str(first+xoffset)+","+num2str(last+xoffset)+", "+ GetWavesDataFolder(wy,2)
		endif
		
		ModifyGraph offset($trace)={0,yoffset};DelayUpdate
		
	endfor
	KillWaves/Z wx_orig
	DoUpdate
	DoAlert/T="X offset" 0, "X offset applied."
End

Function MP_ApplyYOffset(flag)
	Variable flag
	DoAlert/T="Apply Y offset", 1, "Really apply to wave?"
	if(!V_flag)
		Abort
	endif
	GetLastUserMenuInfo
	String list=S_traceName
	String graph=S_graphName
	if(flag)
		list=TraceNameList(WinName(0,1), ";", 1)
		graph=WinName(0,1)
	endif
	Variable i
	for(i=0;i<ItemsInList(list);i+=1)
		String trace=StringFromList(i,list)
		Variable xoffset=0, yoffset=0
		String info=TraceInfo(graph,trace,0)
		Variable pos=strsearch(info, "offset(x)={", 0)
		sscanf info[pos,strlen(info)], "offset(x)={%f,%f%*s", xoffset, yoffset
		if(yoffset==0)
			print trace+" has no Y offset./r"
			continue
		endif
			Wave wy=TraceNameToWaveRef(graph, trace)
			if(strsearch(NameOfWave(wy),"_yoff",0)==-1)
				Wave wy_yoff=$(GetWavesDataFolder(wy,1)+"'"+NameOfWave(wy)+"_yoff'")
				CheckDisplayed/A wy_yoff
				if(V_flag)
					Abort NameOfWave(wy_yoff)+" is in use in another graph. Abort."
				else
					KillWaves/Z wy_yoff
				endif
				Duplicate wy wy_orig
					wy+=yoffset
				String wyname="'"+NameOfWave(wy)+"_yoff'"
				wyname=MP_CheckWaveName(wyname, "Y offset wave name")
				Rename wy $wyname
				String swy_orig=RemoveEnding(GetWavesDataFolder(wy,1)+NameOfWave(wy),"_yoff")
				swy_orig=RemoveEnding(GetWavesDataFolder(wy,1)+"'"+NameOfWave(wy)+"'","_yoff'")
				Duplicate/O wy_orig $swy_orig
				Print GetWavesDataFolder(wy,2)+"+="+num2str(yoffset)
			else
				wy+=yoffset
				Print GetWavesDataFolder(wy,2)+"+="+num2str(yoffset)
			endif
			ModifyGraph offset($trace)={xoffset,0};DelayUpdate
	endfor
	KillWaves/Z wy_orig
	DoUpdate
	DoAlert/T="Y offset" 0, "Y offset applied."
End

Function MP_ApplyYMulOffset(flag)
	Variable flag
	DoAlert/T="Apply Y mulOffset", 1, "Really apply to wave?"
	if(!V_flag)
		Abort
	endif
	GetLastUserMenuInfo
	String list=S_traceName
	String graph=S_graphName
	if(flag)
		list=TraceNameList(WinName(0,1), ";", 1)
		graph=WinName(0,1)
	endif
	Variable i
	for(i=0;i<ItemsInList(list);i+=1)
		String trace=StringFromList(i,list)
		Variable xmuloffset=0, ymuloffset=0
		String info=TraceInfo(graph,trace,0)
		Variable pos=strsearch(info, "muloffset(x)={", 0)
		sscanf info[pos,strlen(info)], "muloffset(x)={%f,%f%*s", xmuloffset, ymuloffset
		if(ymuloffset==0)
			print trace+" has no Y multiplier./r"
			continue
		endif
			Wave wy=TraceNameToWaveRef(graph, trace)
			if(strsearch(NameOfWave(wy),"_ymul",0)==-1)
				Wave wy_ymul=$(GetWavesDataFolder(wy,1)+"'"+NameOfWave(wy)+"_ymul'")
				CheckDisplayed/A wy_ymul
				if(V_flag)
					Abort NameOfWave(wy_ymul)+" is in use in another graph. Abort."
				else
					KillWaves/Z wy_ymul
				endif
				Duplicate wy wy_orig
					wy*=ymuloffset
				String wyname=NameOfWave(wy)+"_ymul"
				wyname=MP_CheckWaveName(wyname, "Y multiplied wave name")
				Rename wy $wyname
				String swy_orig=RemoveEnding(GetWavesDataFolder(wy,1)+NameOfWave(wy),"_ymul")
				swy_orig=RemoveEnding(GetWavesDataFolder(wy,1)+"'"+NameOfWave(wy)+"'","_ymul'")
				Duplicate/O wy_orig $swy_orig
				Print GetWavesDataFolder(wy,2)+"*="+num2str(ymuloffset)
			else
				wy*=ymuloffset
				Print GetWavesDataFolder(wy,2)+"*="+num2str(ymuloffset)
			endif
			ModifyGraph muloffset($trace)={xmuloffset,0};DelayUpdate
	endfor
	KillWaves/Z wy_orig
	DoUpdate
	DoAlert/T="Y multiplier" 0, "Y multiplier applied."
End

Function MP_xoff(flag)
	
	Variable flag
	GetLastUserMenuInfo
	String trace=S_traceName
	
	Variable xoff=0.1
	
	switch(flag)
		case 0:
			Prompt xoff, "Shift by: "
 			DoPrompt/HELP="" "X shift", xoff
 			if(V_flag)
 				Abort "No shift applied."
 			endif
 			Wave wy=TraceNameToWaveRef("", trace)
			Wave wx=XWaveRefFromTrace("", trace)
 			break
		case 1:
			Wave wA=CsrWaveRef(A)
			if(!WaveExists(wA))
				Abort "Cursor[A] is not on the graph!"
			endif
			Wave wy=CsrWaveRef(A)
			Wave wx=CsrXWaveRef(A)
			if(WaveExists(wx))
				xoff=-wx[pcsr(A)]
			else
				xoff=-DimOffset(wy,0)-DimDelta(wy,0)*pcsr(A)
			endif
			break
		case 2:
			Wave wA=CsrWaveRef(A)
			if(!WaveExists(wA))
				Abort "Cursor[A] is not on the graph!"
			endif
			Wave wB=CsrWaveRef(B)
			if(!WaveExists(wB))
				Abort "Cursor[B] is not on the graph!"
			endif
			Wave wy=CsrWaveRef(A)
			Wave wx=CsrXWaveRef(A)
			Wave wxB=CsrXWaveRef(B)
			Wave wyB=CsrWaveRef(B)
			if(WaveExists(wx))
				if(WaveExists(wxB))
					xoff=wxB[pcsr(B)]-wx[pcsr(A)]
				else
					xoff=DimOffset(wyB,0)+DimDelta(wyB,0)*pcsr(B)-wx[pcsr(A)]
				endif
			else
				if(WaveExists(wxB))
					xoff=wxB[pcsr(B)]-(DimOffset(wy,0)+DimDelta(wy,0)*pcsr(A))
				else
					xoff=DimOffset(wyB,0)+DimDelta(wyB,0)*pcsr(B)-(DimOffset(wy,0)+DimDelta(wy,0)*pcsr(A))
				endif
			endif
			break
	endswitch
	
	if(WaveExists(wx))
		if(strsearch(NameOfWave(wx),"_xoff",0)==-1)
			Wave wx_xoff=$(GetWavesDataFolder(wx,1)+"'"+NameOfWave(wx)+"_xoff'")
			CheckDisplayed/A wx_xoff
			if(V_flag)
				Abort NameOfWave(wx_xoff)+" is in use in another graph. Abort."
			else
				KillWaves/Z wx_xoff
			endif
			Duplicate/O wx wx_orig
				wx+=xoff
			String wxname=NameOfWave(wx)+"_xoff"
			wxname=MP_CheckWaveName(wxname, "X offset wave name")
			Rename wx $wxname
			Print GetWavesDataFolder(wx,2)+"+="+num2str(xoff)
			String swx_orig=RemoveEnding(GetWavesDataFolder(wx,1)+NameOfWave(wx),"_xoff")
			swx_orig=RemoveEnding(GetWavesDataFolder(wx,1)+"'"+NameOfWave(wx)+"'","_xoff'")
			Duplicate/O wx_orig $swx_orig
			KillWaves/Z wx_orig
		else
			wx+=xoff
			Print GetWavesDataFolder(wx,2)+"+="+num2str(xoff)
		endif
	else
		Variable first=DimOffset(wy,0)
		Variable last=DimOffset(wy,0)+DimDelta(wy,0)*(numpnts(wy)-1)
		SetScale/I x (first+xoff),(last+xoff), wy
		Print "Setscale//I x "+num2str(first)+","+num2str(last)+", "+ GetWavesDataFolder(wy,2)
		Print "Setscale//I x "+num2str(first+xoff)+","+num2str(last+xoff)+", "+ GetWavesDataFolder(wy,2)
	endif
	
End

Function MP_ymul(flag)
	
	Variable flag
	GetLastUserMenuInfo
	String trace=S_traceName
	
	Variable ymul=0.1
	
	switch(flag)
		case 0:
			Prompt ymul, "Mltiply by: "
 			DoPrompt/HELP="" "Y multiply", ymul
 			if(V_flag)
 				Abort "No multiplication applied."
 			endif
 			Wave py=TraceNameToWaveRef("", trace)
 			break
		case 1:
			Wave wA=CsrWaveRef(A)
			if(!WaveExists(wA))
				Abort "Cursor[A] is not on the graph!"
			endif
			Wave py=CsrWaveRef(A)
			ymul=1/py[pcsr(A)]
			break
		case 2:
			Wave wA=CsrWaveRef(A)
			if(!WaveExists(wA))
				Abort "Cursor[A] is not on the graph!"
			endif
			Wave wB=CsrWaveRef(B)
			if(!WaveExists(wB))
				Abort "Cursor[B] is not on the graph!"
			endif
			Wave py=CsrWaveRef(A)
			Wave pyB=CsrWaveRef(B)
			ymul=pyB[pcsr(B)]/py[pcsr(A)]
			break
	endswitch
	
	if(strsearch(NameOfWave(py),"_ymul",0)==-1)
		Wave py_ymul=$(GetWavesDataFolder(py,1)+"'"+NameOfWave(py)+"_ymul'")
		CheckDisplayed/A py_ymul
		if(V_flag)
			Abort NameOfWave(py_ymul)+" is in use in another graph. Abort."
		else
			KillWaves/Z py_ymul
		endif
		Duplicate/O py py_orig
			py*=ymul
		String pyname=NameOfWave(py)+"_ymul"
		pyname=MP_CheckWaveName(pyname, "Y multiplied wave name")
		Rename py $pyname
		Print GetWavesDataFolder(py,2)+"*="+num2str(ymul)
		String spy_orig=RemoveEnding(GetWavesDataFolder(py,1)+NameOfWave(py),"_ymul")
		spy_orig=RemoveEnding(GetWavesDataFolder(py,1)+"'"+NameOfWave(py)+"'","_ymul'")
		Duplicate/O py_orig $spy_orig
		KillWaves/Z py_orig
	else
		py*=ymul
		Print GetWavesDataFolder(py,2)+"*="+num2str(ymul)
	endif
End

Function MP_yoff(flag)
	
	Variable flag
	GetLastUserMenuInfo
	String trace=S_traceName
	
	Variable yoff=0.1
	
	switch(flag)
		case 0:
			Prompt yoff, "Shift by: "
 			DoPrompt/HELP="" "Y shift", yoff
 			if(V_flag)
 				Abort "No shift applied."
 			endif
 			Wave py=TraceNameToWaveRef("", trace)
 			break
		case 1:
			Wave wA=CsrWaveRef(A)
			if(!WaveExists(wA))
				Abort "Cursor[A] is not on the graph!"
			endif
			Wave py=CsrWaveRef(A)
			yoff=-py[pcsr(A)]
			break
		case 2:
			Wave wA=CsrWaveRef(A)
			if(!WaveExists(wA))
				Abort "Cursor[A] is not on the graph!"
			endif
			Wave wB=CsrWaveRef(B)
			if(!WaveExists(wB))
				Abort "Cursor[B] is not on the graph!"
			endif
			Wave py=CsrWaveRef(A)
			Wave pyB=CsrWaveRef(B)
			yoff=pyB[pcsr(B)]-py[pcsr(A)]
			break
	endswitch
	
	if(strsearch(NameOfWave(py),"_yoff",0)==-1)
		Wave py_yoff=$(GetWavesDataFolder(py,1)+"'"+NameOfWave(py)+"_yoff'")
		CheckDisplayed/A py_yoff
		if(V_flag)
			Abort NameOfWave(py_yoff)+" is in use in another graph. Abort."
		else
			KillWaves/Z py_yoff
		endif
		Duplicate/O py py_orig
			py+=yoff
			String pyname=NameOfWave(py)+"_yoff"
		pyname=MP_CheckWaveName(pyname, "Y offset wave name")
		Rename py $pyname
		Print GetWavesDataFolder(py,2)+"+="+num2str(yoff)
		String spy_orig=RemoveEnding(GetWavesDataFolder(py,1)+NameOfWave(py),"_yoff")
		spy_orig=RemoveEnding(GetWavesDataFolder(py,1)+"'"+NameOfWave(py)+"'","_yoff'")
		Duplicate/O py_orig $spy_orig
		KillWaves/Z py_orig
	else
		py+=yoff
		Print GetWavesDataFolder(py,2)+"+="+num2str(yoff)
	endif
End

Function MP_SetXOffset(flag)
	
	Variable flag
	
	GetLastUserMenuInfo
	String trace=S_traceName
	Variable xoffset=0.1
	Variable xofforig=0
	switch(flag)
		case 0:
			Prompt xoffset, "Shift by: "
 			DoPrompt/HELP="" "X shift", xoffset
 			if(V_flag)
 				Abort "No shift applied."
 			endif
 			Wave wy=TraceNameToWaveRef("", trace)
			Wave wx=XWaveRefFromTrace("", trace)
 			break
		case 1:
			Wave wA=CsrWaveRef(A)
			if(!WaveExists(wA))
				Abort "Cursor[A] is not on the graph!"
			endif
			trace=StringByKey("TNAME", CsrInfo(A))
			Wave wx=CsrWaveRef(A)
			Wave wy=CsrXWaveRef(A)
			if(WaveExists(wx))
				xoffset=-wx[pcsr(A)]
			else
				xoffset=-DimOffset(wy,0)-DimDelta(wy,0)*pcsr(A)
			endif
			break
		case 2:
			Wave wA=CsrWaveRef(A)
			if(!WaveExists(wA))
				Abort "Cursor[A] is not on the graph!"
			endif
			Wave wB=CsrWaveRef(B)
			if(!WaveExists(wB))
				Abort "Cursor[B] is not on the graph!"
			endif
			trace=StringByKey("TNAME", CsrInfo(A))
			Wave wy=CsrWaveRef(A)
			Wave wx=CsrXWaveRef(A)
			Wave wxB=CsrXWaveRef(B)
			Wave wyB=CsrWaveRef(B)
			if(WaveExists(wx))
				if(WaveExists(wxB))
					xoffset=wxB[pcsr(B)]-wx[pcsr(A)]
				else
					xoffset=DimOffset(wyB,0)+DimDelta(wyB,0)*pcsr(B)-wx[pcsr(A)]
				endif
			else
				if(WaveExists(wxB))
					xoffset=wxB[pcsr(B)]-(DimOffset(wy,0)+DimDelta(wy,0)*pcsr(A))
				else
					xoffset=DimOffset(wyB,0)+DimDelta(wyB,0)*pcsr(B)-(DimOffset(wy,0)+DimDelta(wy,0)*pcsr(A))
				endif
			endif
			break
	endswitch
	
	String winrec=WinRecreation("",0)
	String traceB=StringByKey("TNAME", CsrInfo(B))
	Variable pos=strsearch(winrec, "\tModifyGraph offset("+traceB+")",0)
	if(!pos==-1)
		sscanf winrec[pos, strlen(winrec)-1], "\tModifyGraph offset("+traceB+")={%f,%*f%*s", xofforig
	endif
			
	ModifyGraph offset($trace)={xoffset+xofforig,0}
	
End

Function MP_SetYMulOffset(flag)
	
	Variable flag
	
	GetLastUserMenuInfo
	String trace=S_traceName
	Variable ymuloffset=0.1
	Variable ymulorig=0
	switch(flag)
		case 0:
			Prompt ymuloffset, "Mltiply by: "
 			DoPrompt/HELP="" "Y multiply", ymuloffset
 			if(V_flag)
 				Abort "No multiplication applied."
 			endif
 			Wave py=TraceNameToWaveRef("", trace)
 			break
		case 1:
			Wave wA=CsrWaveRef(A)
			if(!WaveExists(wA))
				Abort "Cursor[A] is not on the graph!"
			endif
			trace=StringByKey("TNAME", CsrInfo(A))
			Wave py=CsrWaveRef(A)
			ymuloffset=1/py[pcsr(A)]
			break
		case 2:
			Wave wA=CsrWaveRef(A)
			if(!WaveExists(wA))
				Abort "Cursor[A] is not on the graph!"
			endif
			Wave wB=CsrWaveRef(B)
			if(!WaveExists(wB))
				Abort "Cursor[B] is not on the graph!"
			endif
			trace=StringByKey("TNAME", CsrInfo(A))
			Wave py=CsrWaveRef(A)
			Wave pyB=CsrWaveRef(B)
			ymuloffset=pyB[pcsr(B)]/py[pcsr(A)]
			String winrec=WinRecreation("",0)
			String traceB=StringByKey("TNAME", CsrInfo(B))
			Variable pos=strsearch(winrec, "ModifyGraph muloffset("+traceB+")",0)
			if(!pos==-1)
				sscanf winrec[pos, strlen(winrec)-1], "ModifyGraph muloffset("+traceB+")={%*f,%f%*s", ymulorig
			endif
			break
	endswitch
	
	ModifyGraph muloffset($trace)={0,ymuloffset+ymulorig}
	print "ModifyGraph muloffset("+trace+")={0,"+num2str(ymuloffset+ymulorig)+"}"
End

Function MP_SetYOffset(flag)
	
	Variable flag
	
	GetLastUserMenuInfo
	String trace=S_traceName	
	Variable yoffset=0.1
	Variable yofforig=0
	switch(flag)
		case 0:
			Prompt yoffset, "Shift by: "
 			DoPrompt/HELP="" "Y shift", yoffset
 			if(V_flag)
 				Abort "No shift applied."
 			endif
 			Wave py=TraceNameToWaveRef("", trace)
 			break
		case 1:
			Wave wA=CsrWaveRef(A)
			if(!WaveExists(wA))
				Abort "Cursor[A] is not on the graph!"
			endif
			trace=StringByKey("TNAME", CsrInfo(A))
			Wave py=CsrWaveRef(A)
			yoffset=-py[pcsr(A)]
			break
		case 2:
			Wave wA=CsrWaveRef(A)
			if(!WaveExists(wA))
				Abort "Cursor[A] is not on the graph!"
			endif
			Wave wB=CsrWaveRef(B)
			if(!WaveExists(wB))
				Abort "Cursor[B] is not on the graph!"
			endif
			trace=StringByKey("TNAME", CsrInfo(A))
			Wave py=CsrWaveRef(A)
			Wave pyB=CsrWaveRef(B)
			yoffset=pyB[pcsr(B)]-py[pcsr(A)]
			String winrec=WinRecreation("",0)
			String traceB=StringByKey("TNAME", CsrInfo(B))
			Variable pos=strsearch(winrec, "\tModifyGraph offset("+traceB+")",0)
			if(!pos==-1)
				sscanf winrec[pos, strlen(winrec)-1], "\tModifyGraph offset("+traceB+")={%*f,%f%*s", yofforig
			endif
			break
	endswitch
	
	ModifyGraph offset($trace)={0, yoffset+yofforig}
	
End

//Create new wave from marquee
Function MP_CropWaves(flag)
	Variable flag
	
	GetMarquee/K bottom
	Variable left=V_left
	Variable right=V_right
	
	String graph, list, trace
	if(flag)
		list=TraceNameList(WinName(0,1), ";", 1)
		graph=WinName(0,1)
	else
		if(stringmatch(S_marqueeWin, graph)==0)
			Abort "Before right-click on the trace to activate it."
		endif
		GetLastUserMenuInfo
		graph=S_graphName
		list=S_traceName
		
	endif
	
	String legenda=WinRecreation(graph,0)
	legenda=legenda[strsearch(legenda, "ModifyGraph",0),strlen(legenda)-10]
	legenda=ReplaceString("ModifyGraph", legenda, "ModifyGraph/Z")
	String wycommands=ListMatch(legenda, "\tCursor*", "\r")
	legenda=RemoveFromList(wycommands, legenda, "\r")
	
	Display
	Variable i
	for(i=0;i<ItemsInList(list);i+=1)
		trace=StringFromList(i,list)
		Wave wy=TraceNameToWaveRef(graph, trace)
		Wave wx=XWaveRefFromTrace(graph, trace)
		if(WaveExists(wx))
			Variable dimx=numpnts(wx)
			Duplicate/O wx, wx_crop
			SetScale/I x, wx[0], wx[dimx-1], wx_crop
			Variable p1x=max(0,round(x2pnt(wx_crop, left)))
			Variable p2x=min(round(x2pnt(wx_crop, right)),numpnts(wx_crop)-1)
			Duplicate/O wx, wx_crop
			DeletePoints p2x, dimx-p2x, wx_crop
			DeletePoints 0, p1x, wx_crop
		endif
		Variable dimy=numpnts(wy)
		Duplicate/O wy, wy_crop
		SetScale/I x, wy[0], wy[dimy-1], wy_crop
		Variable p1y=max(0,round(x2pnt(wy_crop, left)))
		Variable p2y=min(round(x2pnt(wy_crop, right)),numpnts(wy_crop)-1)
		Duplicate/O wy, wy_crop
		DeletePoints p2y, dimy-p2y, wy_crop
		DeletePoints 0, p1y, wy_crop
		
		if(numpnts(wy_crop)==0)
			legenda=ReplaceString("*("+trace+")*", legenda, "")
			continue
		endif
		
		SetDataFolder GetWavesDataFolderDFR(wy)
		String wyname=trace+"_"+num2str(p1y)+"_"+num2str(p2y)
		wyname=MP_CheckWaveName(wyname, "Crop Y wave name")
		legenda=ReplaceString("("+trace+")", legenda, "("+wyname+")")
		Duplicate/O wy_crop $wyname
		if(WaveExists(wx))
			String wxname=NameOfWave(wx)+"_"+num2str(p1x)+"_"+num2str(p2x)
			wxname=MP_CheckWaveName(wxname, "Crop X wave name")
			Duplicate/O wx_crop $wxname
			AppendToGraph $wyname vs $wxname
		else
			AppendToGraph $wyname
		endif
	endfor
	for(i=0;i<ItemsInList(legenda, "\r");i+=1)
		Execute StringFromList(i, legenda, "\r")
	endfor
	SetAxis/A
	KillWaves/Z wy_crop, wx_crop
End

Function/S MP_CheckWaveName(wvname, title)
	String wvname
	String title
	String newname=wvname
	if(strlen(newname)>31)
		Prompt newname, "Name of new wave (remove at least "+num2str(strlen(newname)-31)+" chars)"
		DoPrompt/HELP="" title, newname
		newname=MP_CheckWaveName(newname, title)
	endif
	Return newname
End

//Repeat a command with "$" looping "$" from start to stop
Function MP_DoLoop()
	String command="Print $"
	Variable start=0, stop=10, step=1
	Prompt command, "Insert command looping '$' from start to stop: "
	Prompt start, "Start: "
	Prompt stop, "Stop: "
	Prompt step, "Step: "
 	DoPrompt/HELP="" "Do loop", command, start, stop, step
 	if(V_flag)
 		Abort
 	endif
	Variable i=start
	do
		Print ReplaceString("$", command, num2str(i))
		Execute ReplaceString("$", command, num2str(i))
		i+=step
	while(i<=stop)
End

Function MP_LoadTxt()
	
	SetDataFolder root:
	
	String basename="basename"
	Prompt basename, "Base name for the waves"
 	DoPrompt/HELP="" "Load txt", basename
 	
 	NewDataFolder/O/S $basename
 	
 	LoadWave/N=$basename/G/O
 	Variable waves=V_flag
 	
 	Display $(basename+"1") vs $(basename+"0")
 	
 	Variable i
 	for(i=2;i<waves;i+=1)
 		AppendToGraph $(basename+num2str(i)) vs $(basename+"0")
 	endfor
 	Legend
 	
End

Function MP_MathWaves(flag, oper)
	
	Variable flag
	String oper
	
	Wave yB=CsrWaveRef(B)
	if(!WaveExists(yB))
		Abort "Cursor[B] is not on the graph! Abort."
	endif
	Wave xB=CsrXWaveRef(B)
	
	if(flag)
		Wave yA=CsrWaveRef(A)
		if(!WaveExists(yA))
			Abort "Cursor[A] is not on the graph! Abort."
		endif
		Wave xA=CsrXWaveRef(A)
		if(WaveRefsEqual(yA,yB))
			Abort "Cursor[A] and Cursor[B] are on the same wave! Abort."
		endif
	else
		GetLastUserMenuInfo
		String trace=S_traceName
		Wave yA=TraceNameToWaveRef("", trace)
		Wave xA=XWaveRefFromTrace("", trace)
	endif
	
	String syB_CS, pyB_CS
	
	if(WaveExists(xA))
		if(WaveRefsEqual(xA,xB))
			Wave yB_CS=yB
			syB_CS=NameOfWave(yB)
		else
			syB_CS=NameOfWave(yB)+"_CS"
			syB_CS=MP_CheckWaveName(syB_CS, "Cursor B interpolated wave name")
			pyB_CS=GetWavesDataFolder(yB,1)+"'"+syB_CS+"'"
			Duplicate/O yB, $pyB_CS
			Wave yB_CS=$pyB_CS
			if(WaveExists(xB))
				Interpolate2/T=2/E=2/I=3/Y=yB_CS/X=xA xB, yB
			else
				Interpolate2/T=2/E=2/I=3/Y=yB_CS/X=xA yB
			endif
		endif
	else
		Duplicate/O yA, xAtmp
		xAtmp=x
		syB_CS=NameOfWave(yB)+"_CS"
		syB_CS=MP_CheckWaveName(syB_CS, "Cursor B interpolated wave name")
		pyB_CS=GetWavesDataFolder(yB,1)+"'"+syB_CS+"'"
		Duplicate/O yB, $pyB_CS
		Wave yB_CS=$pyB_CS
		if(WaveExists(xB))
			Interpolate2/T=2/E=2/I=3/Y=yB_CS/X=xAtmp xB, yB
		else
			Duplicate/O yB, xBtmp
			xBtmp=x
			Interpolate2/T=2/E=2/I=3/Y=yB_CS/X=xAtmp xBtmp, yB
		endif
	endif
	
	Duplicate/O yA, y_tmp
	String wvname=NameOfWave(yA)+"_"+oper+"_"+syB_CS
	wvname=MP_CheckWaveName(wvname, "New wave name")
	String wvpath=GetWavesDataFolder(yA,1)+"'"+wvname+"'"
	
	strswitch(oper)
		case "plus":
			y_tmp=yA+yB_CS
			Print wvname+"="+NameOfWave(yA)+"+"+syB_CS
			break
		case "minus":
			y_tmp=yA-yB_CS
			Print wvname+"="+NameOfWave(yA)+"-"+syB_CS
			break
		case "mult":
			y_tmp=yA*yB_CS
			Print wvname+"="+NameOfWave(yA)+"*"+syB_CS
			break
		case "div":
			y_tmp=yA/yB_CS
			Print wvname+"="+NameOfWave(yA)+"/"+syB_CS
			break
	endswitch
	
	Duplicate/O y_tmp, $wvpath
	Wave wv=$wvpath
	if(WaveExists(xA))
		AppendToGraph wv vs xA
	else
		AppendToGraph wv
	endif
	KillWaves/Z y_tmp,yB_CS
	
End

Function MP_SmoothWave()
	
	GetLastUserMenuInfo
	String trace=S_traceName
	Wave wy=TraceNameToWaveRef("", trace)
	Wave wx=XWaveRefFromTrace("", trace)
	
	String method="Binomial"
	Variable pnts=1
	Prompt method, "Smooth method", popup "Binomial;Box"
	Prompt pnts, "Points"
	DoPrompt/HELP="" "Smooth"+NameOfWave(wy), method, pnts
	
	Duplicate/O wy, wy_tmp
	
	strswitch(method)
		case "Box":
			Smooth/B pnts, wy_tmp
			break
		case "Binomial":
			Smooth pnts, wy_tmp
			break
	endswitch
	
	String wy_smth=NameOfWave(wy)+"_smth"
	wy_smth=MP_CheckWaveName(wy_smth, "Smoothed wave name")
	String pwy_smth=GetWavesDataFolder(wy,1)+"'"+wy_smth+"'"
	Duplicate/O wy_tmp $pwy_smth
	if(WaveExists(wx))
		AppendToGraph $pwy_smth vs wx
	else
		AppendToGraph $pwy_smth
	endif
End

Function MP_DisplayProceduresHelp()
	DoWindow/K ProceduresHelpNB
	NewNotebook/K=1/N=ProceduresHelpNB/F=0/OPTS=3 as "Procedures Help"
	String helpstr=ProcedureText("MP_ProceduresHelp")
	helpstr=RemoveListItem(0, helpstr, "\r")
	helpstr=RemoveListItem(ItemsInList(helpstr, "\r")-1, helpstr, "\r")
	helpstr=ReplaceString("// ", helpstr, "")
	Notebook ProceduresHelpNB, text=helpstr
End